//
//  ArticlesController.h
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCActionController.h"

@class PCText;

@interface PCArticlesController : UITableViewController<UIWebViewDelegate>

/** title of controller */
@property NSString *name;
/** list of images connected to articles */
@property NSArray *images;
/** list of texts connected to articles */
@property NSArray *texts;
/** list of articles */
@property NSArray *articles;

/** list of articles */
@property NSArray *videos;


/** hide images */
@property NSNumber *hideImage;

@end
