//
//  ArticlesController.m
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCArticlesController.h"
#import "UIImageView+AFNetworking.h"

#import "PCImage.h"
#import "PCText.h"
#import "PCImage.h"
#import "PCArticle.h"
#import "PCVideo.h"
#import "PCGraphicManager.h"
#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "NSString+HTML.h"
#import "PCDefaults.h"

#import "PCPageController.h"

@interface PCArticlesController ()

@property ( strong, nonatomic) PCActionController *actionMenu;

@property ( strong, nonatomic) PCText *proluge;
@property ( strong, nonatomic) PCImage *prolugeImage;
@property ( strong, nonatomic) NSArray *articlesList;

@end

@implementation PCArticlesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];
    
    // init views
    
    self.tableView.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    
    self.tableView.estimatedSectionHeaderHeight = [[PCTitleManager sharedManager] titleHeight];
    
    self.tableView.separatorColor = [[PCGraphicManager sharedManager] contentTextColor];
    
    // find proluge
    
    for (PCText *text in self.texts)
    {
        if ([text.idArticle integerValue] == 0)
        {
            self.proluge = text;
        }
    }
    
    for (PCImage *image in self.images)
    {
        if ([image.idArticle integerValue] == 0)
        {
            self.prolugeImage = image;
        }
    }
    
    // find articles details
    
    for (PCArticle *article in self.articles)
    {
        NSMutableArray *images = [[NSMutableArray alloc] init];
        for (PCImage *image in self.images)
        {
            if ([article.identificator integerValue] == [image.idArticle integerValue])
            {
                [images addObject:image];
            }
        }
        article.images = images;
        
        NSMutableArray *texts = [[NSMutableArray alloc] init];
        for (PCText *text in self.texts)
        {
            if ([article.identificator integerValue] == [text.idArticle integerValue])
            {
                [texts addObject:text];
            }
        }
        article.texts = texts;
        
        NSMutableArray *videos = [[NSMutableArray alloc] init];
        for (PCVideo *video in self.videos)
        {
            if ([article.identificator integerValue] == [video.idArticle integerValue])
            {
                [videos addObject:video];
            }
        }
        article.videos = videos;
    }
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    
    self.articlesList = [self.articles sortedArrayUsingDescriptors:@[sortDescriptor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.articlesList.count + 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // test proluge
    if (indexPath.row == 0)
    {
        if ((self.prolugeImage == nil) || ([self.prolugeImage.url isEqualToString:@""]))
        {
            return 0;
        }
    }
    
    
    // test proluge
    if (indexPath.row == 1)
    {
        if (self.proluge == nil)
        {
            return 0;
        }
        
        if (self.proluge.height != nil)
        {
            return [self.proluge.height floatValue];
        }
        else {
            return 0;
        }
    }
    
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // test proluge
    if (indexPath.row == 0)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellProlugeImage"];
        
        [cell.contentView setNeedsLayout];
        [cell.contentView layoutIfNeeded];
        
        cell.backgroundColor = [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.separatorInset = UIEdgeInsetsMake(0, self.tableView.frame.size.width, 0, 0);
        
        if ((self.prolugeImage == nil) || ([self.prolugeImage.url isEqualToString:@""]))
        {
            return cell;
        }
        
        // init view
        
        UIImageView *image = [[UIImageView alloc] init];
        image.contentMode = UIViewContentModeScaleAspectFit;
        image.clipsToBounds = YES;
        image.translatesAutoresizingMaskIntoConstraints = NO;
        
        // load data
        
        UIImage *img = [[PCGraphicManager sharedManager] imageForURL:self.prolugeImage.url];
        
        image.image = img;
        
        [cell.contentView addSubview:image];
        
        // calculate height of image
        
        float scale = 0;
        
        if (img != nil)
        {
            scale = img.size.height/img.size.width;
        }
        
        float height = scale * cell.contentView.frame.size.width;
        
        // set contrainsts
        
        NSDictionary *bindings = NSDictionaryOfVariableBindings(image);
        
        NSString *str = [NSString stringWithFormat:@"V:|-[image(%.1f@900)]-|", height];
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:str options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[image]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] updateConstraints];
        
        return cell;
    }
    
    // test proluge
    if (indexPath.row == 1)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellProluge"];
        
        [cell.contentView setNeedsLayout];
        [cell.contentView layoutIfNeeded];
        
        cell.backgroundColor = [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ((self.proluge == nil) || ([self.proluge.content isEqualToString:@""]))
        {
            return cell;
        }
        
        // init view
        
        UIWebView *webView = [[UIWebView alloc] init];
        
        webView.translatesAutoresizingMaskIntoConstraints = NO;
        webView.scrollView.scrollEnabled = NO;
        webView.scrollView.bounces = NO;
        
        webView.backgroundColor = [UIColor clearColor];
        webView.opaque = NO;
        webView.scalesPageToFit = NO;
        webView.dataDetectorTypes = UIDataDetectorTypePhoneNumber;
        
        webView.delegate = self;
        
        [cell.contentView addSubview:webView];
        
        // set contrainsts
        
        NSDictionary *bindings = NSDictionaryOfVariableBindings(webView);
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[webView]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[webView]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] updateConstraints];
        
        // load data
        
        NSString *s;
        NSString *css = [[PCDefaults sharedInstance] getCss];
        if ((css == nil) || ([css isEqualToString:@""]))
        {
            s = [NSString stringWithFormat:@"<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px 10px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;}  </style>", [PCGraphicManager sharedManager].contentTextColorHEX ];
        }
        else {
            s = [NSString stringWithFormat:@"<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px 10px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;} %@ </style>", [PCGraphicManager sharedManager].contentTextColorHEX, css ];
        }
        
        NSString *str = [NSString stringWithFormat:@"<html> <body style=\"background-color:transparent; color:%@ \"> %@ %@  </body></html>", [PCGraphicManager sharedManager].contentTextColorHEX , s, self.proluge.content];
        
        [webView loadHTMLString:str baseURL:nil];
        
        return cell;
    }
    
    // test type of cell

    switch (self.hideImage.integerValue)
    {
        case 0:
            return [self cellType0WithIndexPath:indexPath];
            break;
        case 1:
            return [self cellType1WithIndexPath:indexPath];
            break;
        case 2:
            return [self cellType2WithIndexPath:indexPath];
            break;
        case 3:
            return [self cellType3WithIndexPath:indexPath];
            break;
            
        default:
            break;
    }
    
    return nil;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [[PCTitleManager sharedManager] createTitleRowWithTitle:self.name];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // test proluge
    if ((indexPath.row == 0) || (indexPath.row == 1))
    {
        return;
    }
    
    PCArticle *article = self.articlesList[indexPath.row - 2];
    
    PCPageController *pageController = [[PCPageController alloc] init];
    pageController.images = article.images;
    pageController.texts = article.texts;
    pageController.name = article.name;
    pageController.videos = article.videos;
    
    [self.navigationController pushViewController:pageController animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - cell type

-(UITableViewCell*)cellType0WithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellArticle0"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    // init views
    
    UILabel *labelTitle = [[UILabel alloc] init];
    labelTitle.numberOfLines = 2;
    labelTitle.font = [UIFont boldSystemFontOfSize:18.0];
    labelTitle.adjustsFontSizeToFitWidth=YES;
    labelTitle.minimumScaleFactor = 0.5;
    labelTitle.textColor = [PCGraphicManager sharedManager].contentTextColor;
    
    UIImageView *image = [[UIImageView alloc] init];
    image.contentMode = UIViewContentModeScaleAspectFill;
    image.clipsToBounds = YES;
    
    UILabel *labelDescription = [[UILabel alloc] init];
    labelDescription.numberOfLines = 3;
    labelDescription.font = [UIFont systemFontOfSize:12.0];
    labelDescription.textColor = [PCGraphicManager sharedManager].contentTextColor;
    
    labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
    labelDescription.translatesAutoresizingMaskIntoConstraints = NO;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:image];
    [cell.contentView addSubview:labelTitle];
    [cell.contentView addSubview:labelDescription];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(image, labelTitle, labelDescription);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[image(80)]-|" options:0 metrics:nil views:bindings]];
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelTitle(28)]-[labelDescription]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[image(70)]-[labelTitle]-|" options:0 metrics:nil views:bindings]];
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[image]-[labelDescription]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    // load data
    
    PCArticle *article = self.articlesList[indexPath.row - 2];
    
    labelTitle.text = article.name;
    
    if (article.images.count != 0)
    {
        PCImage *img = article.images[0];

        image.image = [[PCGraphicManager sharedManager] imageForURL:img.url];
    }
    
    if (article.texts.count != 0)
    {
        PCText *txt = article.texts[0];
        
        NSString *decodedString = [txt.content stringByConvertingHTMLToPlainText];
        
        labelDescription.text = decodedString;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(UITableViewCell*)cellType1WithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellArticle1"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    // init views
    
    UILabel *labelTitle = [[UILabel alloc] init];
    labelTitle.numberOfLines = 2;
    labelTitle.font = [UIFont boldSystemFontOfSize:18.0];
    labelTitle.adjustsFontSizeToFitWidth=YES;
    labelTitle.minimumScaleFactor = 0.5;
    labelTitle.textColor = [PCGraphicManager sharedManager].contentTextColor;
    
    UILabel *labelDescription = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, 50, 200)];
    labelDescription.numberOfLines = 3;
    labelDescription.font = [UIFont systemFontOfSize:12.0];
    labelDescription.textColor = [PCGraphicManager sharedManager].contentTextColor;
    
    labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
    labelDescription.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:labelTitle];
    [cell.contentView addSubview:labelDescription];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings( labelTitle, labelDescription);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelTitle(28)]-[labelDescription]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[labelTitle]-|" options:0 metrics:nil views:bindings]];
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[labelDescription]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    // load data
    
    PCArticle *article = self.articlesList[indexPath.row - 2];
    
    labelTitle.text = article.name;
    
    if (article.texts.count != 0)
    {
        PCText *txt = article.texts[0];
        
        NSString *decodedString = [txt.content stringByConvertingHTMLToPlainText];
        
        labelDescription.text = decodedString;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(UITableViewCell*)cellType2WithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellArticle2"];
    
    // init views
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    UILabel *labelTitle = [[UILabel alloc] init];
    labelTitle.numberOfLines = 2;
    labelTitle.font = [UIFont boldSystemFontOfSize:18.0];
    labelTitle.adjustsFontSizeToFitWidth=YES;
    labelTitle.minimumScaleFactor = 0.5;
    labelTitle.textColor = [PCGraphicManager sharedManager].contentTextColor;
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
    
    image.contentMode = UIViewContentModeScaleAspectFill;
    image.clipsToBounds = YES;
    
    labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:image];
    [cell.contentView addSubview:labelTitle];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(image, labelTitle);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[image(80)]-|" options:0 metrics:nil views:bindings]];
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelTitle]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[image(70)]-[labelTitle]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    // load data
    
    PCArticle *article = self.articlesList[indexPath.row - 2];
    
    labelTitle.text = article.name;
    
    if (article.images.count != 0)
    {
        PCImage *img = article.images[0];
        
        image.image = [[PCGraphicManager sharedManager] imageForURL:img.url];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(UITableViewCell*)cellType3WithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellArticle3"];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    // init views
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    UILabel *labelTitle = [[UILabel alloc] init];
    labelTitle.numberOfLines = 0;
    labelTitle.font = [UIFont boldSystemFontOfSize:18.0];
    labelTitle.lineBreakMode = NSLineBreakByWordWrapping;
    labelTitle.textColor = [PCGraphicManager sharedManager].contentTextColor;
    
    labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:labelTitle];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings( labelTitle);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelTitle]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[labelTitle]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    // load data
    
    PCArticle *article = self.articlesList[indexPath.row - 2];
    
    labelTitle.text = article.name;
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

#pragma marrk - UIWebView delegate

- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    if (self.proluge.height != nil)
    {
        return;
    }
    
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    aWebView.frame = frame;
    
    self.proluge.height = [NSNumber numberWithFloat:frame.size.height + 2 + 8 + 8];
    
    [UIView setAnimationsEnabled:NO];
    [self.tableView reloadData];
    [UIView setAnimationsEnabled:YES];
    
//    //    [UIView setAnimationsEnabled:NO];
//    [self.tableView beginUpdates];
//    [self.tableView endUpdates];
//    //    [UIView setAnimationsEnabled:YES];
}

@end
