//
//  BarMenuController.h
//  pepi
//
//  Created by Radek Zmeskal on 13/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCAppDescription.h"
#import "PCBarMenuMoreControllerDelegate.h"

@interface PCBarMenuController : UITabBarController <UITabBarControllerDelegate>

/** app description */
@property (strong, nonatomic) PCAppDescription *appDescription;

/**
 *  show curl view
 *
 *  @param sender sender
 */
- (IBAction)curlShowView:(id)sender;

@end

@interface PCBarMenuInfoController : UIViewController

@end
