//
//  BarMenuController.m
//  pepi
//
//  Created by Radek Zmeskal on 13/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCBarMenuController.h"
#import "PCSection.h"
#import "PCGraphicManager.h"
#import "PCBarMenuMoreControllerDelegate.h"
#import "PCEmptyController.h"
#import "PCDefaults.h"

@interface PCBarMenuController ()

@property ( strong, nonatomic) NSMutableArray *controllers;

@property ( strong, nonatomic) PCBarMenuMoreControllerDelegate *moreDelegate;

@property UIView *infoView;

@end

@implementation PCBarMenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // init info view
    
    self.infoView = [[PCDefaults sharedInstance] getInfoController].view;
    
    if (self.infoView != nil)
    {
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(curlHideView:)];
        [self.infoView addGestureRecognizer:singleFingerTap];
    }
    
    // init views
    
    UITableViewController *moreController = (UITableViewController*)self.moreNavigationController.topViewController;
    moreController.view.backgroundColor = [PCGraphicManager sharedManager].menuTextColor;
    
    UITableView *tableView = (UITableView*)moreController.view;
    
    self.moreDelegate = [[PCBarMenuMoreControllerDelegate alloc] initWithForwardingDelegate:tableView.delegate];
    tableView.delegate = self.moreDelegate;
    self.moreDelegate.tabBar = self;
    
    self.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // init tabbar
    
    self.tabBar.translucent = NO;
    self.tabBar.barTintColor = [PCGraphicManager sharedManager].menuColor;
    self.tabBar.tintColor = [PCGraphicManager sharedManager].menuTextColor;
    
    self.moreNavigationController.navigationBar.translucent = NO;
    self.moreNavigationController.navigationBar.barTintColor = [PCGraphicManager sharedManager].menuColor;
    self.moreNavigationController.navigationBar.tintColor = [PCGraphicManager sharedManager].menuTextColor;
    [self.moreNavigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [PCGraphicManager sharedManager].menuTextColor}];
    self.moreNavigationController.topViewController.view.backgroundColor = [PCGraphicManager sharedManager].menuColor;
    
    // load data
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    
    NSArray *array = [self.appDescription.sections sortedArrayUsingDescriptors:@[sortDescriptor]];
    
//    [[UIView appearanceWhenContainedIn:[UITabBar class], nil] setTintColor:[UIColor yellowColor]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[PCGraphicManager sharedManager].menuTextColor, NSForegroundColorAttributeName, nil] forState:UIControlStateHighlighted];
    
    for (PCSection *section in array)
    {
        [section createNavigationController];
        
        UIEdgeInsets insets = UIEdgeInsetsMake(5, 5, 5, 5);
        UIImage *imag = [[PCGraphicManager sharedManager] imageForURL:section.icon];
        UIImage *imag2 = [PCGraphicManager imageWithImage:imag convertToSize:CGSizeMake(30, 30)];
//        [imag drawInRect:CGRectMake(0, 0, 40, 40)];
        section.navigationController.tabBarItem.image = imag2;
//        section.navigationController.tabBarItem.selectedImage = imag2;
//        section.navigationController.tabBarItem.imageInsets = insets;
        section.navigationController.tabBarItem.title = section.name;
        
//        [section.navigationController.tabBarItem set]
    }
    
    self.controllers = [[NSMutableArray alloc] init];
    for (PCSection *section in array)
    {
        [self.controllers addObject:section.navigationController];
    }
    
    if (self.controllers.count == 0)
    {
        PCEmptyController *emptyController = [[PCEmptyController alloc] init];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:emptyController];
        navigationController.navigationBar.translucent = NO;
        navigationController.navigationBar.barTintColor = [PCGraphicManager sharedManager].headerColor;
        navigationController.navigationBar.tintColor = [PCGraphicManager sharedManager].headerTextColor;
        [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [PCGraphicManager sharedManager].headerTextColor}];
        
        
        UIEdgeInsets insets = UIEdgeInsetsMake(2, 2, 2, 2);
//        UIImage *imag = [[PCGraphicManager sharedManager] imageForURL:section.icon];
//        navigationController.tabBarItem.image = imag;
        navigationController.tabBarItem.imageInsets = insets;
        navigationController.tabBarItem.title = @"Úvod";
        
        [self.controllers addObject:navigationController];
    }
    
    if (self.infoView != nil)
    {
        PCBarMenuInfoController *info = [[PCBarMenuInfoController alloc] init];
        UIEdgeInsets insets = UIEdgeInsetsMake(2, 2, 2, 2);
        info.tabBarItem.image = [UIImage imageNamed:@"pepi_info"];
        info.tabBarItem.imageInsets = insets;
        info.tabBarItem.title = @"O aplikaci";
        [self.controllers addObject:info];

        [self setViewControllers:self.controllers];
        
        NSMutableArray *customizableViewControllers = [NSMutableArray arrayWithArray:self.customizableViewControllers];
        [customizableViewControllers removeObject:info];
        self.customizableViewControllers = customizableViewControllers;

        return;
    }
    
    [self setViewControllers:self.controllers];
    
//    NSMutableArray *customizableViewControllers = [NSMutableArray arrayWithArray:self.customizableViewControllers];
//    self.customizableViewControllers = customizableViewControllers;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(nonnull UIViewController *)viewController
{
    if ([viewController isKindOfClass:[PCBarMenuInfoController class]])
    {
        [self performSelector:@selector(curlShowView:) withObject:nil];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - CURL functionality

- (IBAction)curlShowView:(id)sender
{
    
    [UIView animateWithDuration:1.0
                     animations:^{
                         CATransition *animation = [CATransition animation];
//                         [animation setDelegate:self];
                         [animation setDuration:0.7];
                         [animation setTimingFunction:[CAMediaTimingFunction functionWithName:@"default"]];
                         animation.type = @"pageCurl";
                         animation.fillMode = kCAFillModeForwards;
                         animation.endProgress = 0.6;
                         [animation setRemovedOnCompletion:NO];
                         [self.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
                         
                         [self.view addSubview:self.infoView];
                         
                         NSDictionary *bindings = [NSDictionary dictionaryWithObject:self.infoView forKey:@"curlView"];
                         
                         [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[curlView]|" options:0 metrics:nil views:bindings]];
                         [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[curlView]|" options:0 metrics:nil views:bindings]];
                     }
     ];
}

- (IBAction)curlHideView:(id)sender {
    [UIView animateWithDuration:1.0
                     animations:^{
                         CATransition *animation = [CATransition animation];
//                         [animation setDelegate:self];
                         [animation setDuration:0.7];
                         [animation setTimingFunction:[CAMediaTimingFunction functionWithName:@"default"]];
                         animation.type = @"pageUnCurl";
                         animation.fillMode = kCAFillModeForwards;
                         animation.startProgress = 0.35;
                         [animation setRemovedOnCompletion:NO];
                         [self.view.layer addAnimation:animation forKey:@"pageUnCurlAnimation"];
                         [self.infoView removeFromSuperview];
                         ;}
     ];
}

@end

@implementation PCBarMenuInfoController

@end
