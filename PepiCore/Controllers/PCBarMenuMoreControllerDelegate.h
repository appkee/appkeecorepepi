//
//  BarMenuMoreController.h
//  pepi
//
//  Created by Radek Zmeskal on 19/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PCBarMenuMoreControllerDelegate : NSObject <UITableViewDelegate>

/** tabbar controller */
@property UITabBarController *tabBar;

- (instancetype)initWithForwardingDelegate:(id<UITableViewDelegate>)delegate;

@end
