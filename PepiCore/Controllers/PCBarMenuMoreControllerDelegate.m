//
//  BarMenuMoreController.m
//  pepi
//
//  Created by Radek Zmeskal on 19/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCBarMenuMoreControllerDelegate.h"
#import "PCGraphicManager.h"
#import "PCBarMenuController.h"

@interface PCBarMenuMoreControllerDelegate ()

@property (nonatomic, strong) id<UITableViewDelegate> forwardingDelegate;

@property ( strong, nonatomic) UIView *curlView;

@end


@implementation PCBarMenuMoreControllerDelegate


- (instancetype)initWithForwardingDelegate:(id<UITableViewDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.forwardingDelegate = delegate;
    }
    return self;
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    if ([[self class] instancesRespondToSelector:aSelector]) {
        return YES;
    }
    return [self.forwardingDelegate respondsToSelector:aSelector];
}

- (id)forwardingTargetForSelector:(SEL)aSelector
{
    return self.forwardingDelegate;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *controller = self.tabBar.viewControllers[indexPath.row + 4];    
    
    if ([controller isKindOfClass:[PCBarMenuInfoController class]])
    {
        [self.tabBar performSelector:@selector(curlShowView:) withObject:nil];
        
        return nil;
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.forwardingDelegate respondsToSelector:_cmd]) {
        [self.forwardingDelegate tableView:tableView willDisplayCell:cell forRowAtIndexPath:indexPath];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [PCGraphicManager sharedManager].menuTextColor;
    
    cell.imageView.tintColor = [PCGraphicManager sharedManager].menuTextColor;
}

@end
