//
//  EmptyController.m
//  pepi
//
//  Created by Radek Zmeskal on 05/12/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCEmptyController.h"
#import "PCNavigationManager.h"
#import "PCActionController.h"
#import "PCGraphicManager.h"

@interface PCEmptyController ()

@property ( strong, nonatomic) PCActionController *actionMenu;

@end

@implementation PCEmptyController

- (void)viewDidLoad {
    [super viewDidLoad];

    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];
    
    // init views
    
    self.view.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.numberOfLines = 0;
    label1.font = [UIFont boldSystemFontOfSize:21];
    label1.textColor = [[PCGraphicManager sharedManager] contentTextColor];
    label1.textAlignment = NSTextAlignmentCenter;
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.numberOfLines = 0;
    label2.font = [UIFont systemFontOfSize:21];
    label2.textColor = [[PCGraphicManager sharedManager] contentTextColor];
    label2.textAlignment = NSTextAlignmentCenter;
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.numberOfLines = 0;
    label3.font = [UIFont systemFontOfSize:16];
    label3.textColor = [[PCGraphicManager sharedManager] contentTextColor];
    label3.textAlignment = NSTextAlignmentCenter;
    
    label1.translatesAutoresizingMaskIntoConstraints = NO;
    label2.translatesAutoresizingMaskIntoConstraints = NO;
    label3.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:label1];
    [self.view addSubview:label2];
    [self.view addSubview:label3];
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(label1, label2, label3);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[label1(>=30)]-20-[label2(>=30)]-20-[label3(>=30)]" options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label1]-|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label2]-|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label3]-|" options:0 metrics:nil views:bindings]];
    
    [self.view updateConstraints];
    
    label1.text = @"Nemáte zatím žádné vlastní záložky.";
//    label2.text = @"Pro přidání vlastních záložek navštivte webové rohraní.";
//    label3.text = @"http://pepiapp.cz.uvirt43.active24.cz/";    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
