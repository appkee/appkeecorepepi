//
//  FormController.h
//  pepi
//
//  Created by Radek Zmeskal on 16/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCActionController.h"

@interface PCFormController : UITableViewController< UITextViewDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property NSString *name;
@property NSNumber *section;
@property NSString *confirm;
@property NSString *email;
@property NSArray *fileds;
@property NSArray *fieldsItems;

@end
