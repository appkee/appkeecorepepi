//
//  FormController.m
//  pepi
//
//  Created by Radek Zmeskal on 16/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCFormController.h"

#import "PCField.h"
#import "PCFieldsItem.h"
#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "PCGraphicManager.h"
#import "PCDataManager.h"

#define SPACE 12.0

@interface PCFormController ()

@property ( strong, atomic) PCActionController *actionMenu;

@property ( strong, atomic) NSArray *fields;

@property ( strong, atomic) UITextView *textActual;
@property ( strong, atomic) UITextField *textFieldActual;

@end

@implementation PCFormController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];

    // init views
    
    self.tableView.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200;
    self.tableView.estimatedSectionHeaderHeight = [[PCTitleManager sharedManager] titleHeight];

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.allowsSelection = NO;
    
    // load data
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    
    self.fields = [self.fileds sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    for (PCField *field in self.fields)
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (PCFieldsItem *item in self.fieldsItems)
        {
            if ([field.identificator integerValue] == [item.idField integerValue])
            {
                [array addObject:item];
            }
        }
        
        if (array.count != 0)
        {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
            
            field.items = [array sortedArrayUsingDescriptors:@[sortDescriptor]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fields.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // test send button
    if (indexPath.row == self.fields.count)
    {
        UITableViewCell *cell = [self cellForSendButtonWithIndexPath:indexPath];
        
        return cell;
    }
    
    // load table view cell
    PCField *field = self.fields[indexPath.row];
    
    if ([field.type isEqualToString:@"nadpis"])
    {
        UITableViewCell *cell = [self cellForTitleWithIndexPath:indexPath];
        
        return cell;
    }
    
    if ([field.type isEqualToString:@"text"])
    {
        UITableViewCell *cell = [self cellForInputWithIndexPath:indexPath];
        
        return cell;
    }

    if ([field.type isEqualToString:@"textarea"])
    {
        UITableViewCell *cell = [self cellForInputAreaWithIndexPath:indexPath];
        
        return cell;
    }

    if ([field.type isEqualToString:@"number"])
    {
        UITableViewCell *cell = [self cellForInputNumberWithIndexPath:indexPath];
        
        return cell;
    }

    if ([field.type isEqualToString:@"checkbox"])
    {
        UITableViewCell *cell = [self cellForPickerWithIndexPath:indexPath];
        
        return cell;
    }
    
    if ([field.type isEqualToString:@"select"])
    {
        UITableViewCell *cell = [self cellForSelectWithIndexPath:indexPath];
        
        return cell;
    }

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];    
    
    return cell;
}


//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return [[TitleManager sharedManager] titleHeight];
//}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [[PCTitleManager sharedManager] createTitleRowWithTitle:self.name];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - table view cells


-(UITableViewCell*)cellForTitleWithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellTitle"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    // init views
    
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [PCGraphicManager sharedManager].contentTextColor;
    label.font = [UIFont boldSystemFontOfSize:20];
    label.numberOfLines = 0;
    
    UIImageView *line = [[UIImageView alloc] init];
    line.backgroundColor = [PCGraphicManager sharedManager].contentTextColor;
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    line.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:line];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(label, line);
    
    NSString *heigth = [NSString stringWithFormat:@"V:|-%.1f@900-[label][line(1)]-%.1f@900-|", SPACE, SPACE];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heigth options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[line]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView updateConstraints];
    
    // load data
    
    PCField *field = self.fields[indexPath.row];
    
    label.text = field.name;
    
    return cell;
}

-(UITableViewCell*)cellForInputWithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellInput"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    // init views
    
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [PCGraphicManager sharedManager].contentTextColor;
    label.font = [UIFont boldSystemFontOfSize:18];
    label.numberOfLines = 0;
    
    UITextField *text = [[UITextField alloc] init];
    text.layer.borderColor = [PCGraphicManager sharedManager].contentTextColor.CGColor;
    text.layer.borderWidth = 1;
    text.backgroundColor = [UIColor whiteColor];
    text.returnKeyType = UIReturnKeyDone;
    
    text.delegate = self;
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    text.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:text];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(label, text);
    
    NSString *heigth = [NSString stringWithFormat:@"V:|-%.1f@900-[label]-[text(30)]-%.1f@900-|", SPACE, SPACE];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heigth options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[text]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView updateConstraints];
    
    // load data
    
    PCField *field = self.fields[indexPath.row];
    
    label.text = field.name;
    text.text = field.value;
    
    text.tag = indexPath.row;
    
    return cell;
}

-(UITableViewCell*)cellForInputAreaWithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellInputArea"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    // init views
    
    UILabel *label = [[UILabel alloc] init];
    
    label.textColor = [PCGraphicManager sharedManager].contentTextColor;
    label.font = [UIFont boldSystemFontOfSize:18];
    label.numberOfLines = 0;
    
    UITextView *text = [[UITextView alloc] init];
    text.backgroundColor = [UIColor whiteColor];
    text.layer.borderColor = [PCGraphicManager sharedManager].contentTextColor.CGColor;
    text.layer.borderWidth = 1;
    text.delegate = self;
    
    // init toolbar
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, cell.bounds.size.width, 44)];
    toolBar.translucent = NO;
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(clickButtonDone:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpace, btnDone, nil]];
    toolBar.contentMode = UIViewContentModeRight;
    
    toolBar.barTintColor = [PCGraphicManager sharedManager].headerColor;
    btnDone.tintColor = [PCGraphicManager sharedManager].headerTextColor;
    
    [text setInputAccessoryView:toolBar];
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    text.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:text];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(label, text);
    
    NSString *heigth = [NSString stringWithFormat:@"V:|-%.1f@900-[label]-[text(100)]-%.1f@900-|", SPACE, SPACE];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heigth options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[text]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView updateConstraints];
    
    // load data
    
    PCField *field = self.fields[indexPath.row];
    
    label.text = field.name;
    text.text = field.value;
    
    text.tag = indexPath.row;
    
    return cell;
}

-(UITableViewCell*)cellForInputNumberWithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellInput"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    // init views
    
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [PCGraphicManager sharedManager].contentTextColor;
    label.font = [UIFont boldSystemFontOfSize:18];
    label.numberOfLines = 0;
    
    UITextField *text = [[UITextField alloc] init];
    text.backgroundColor = [UIColor whiteColor];
    text.layer.borderColor = [PCGraphicManager sharedManager].contentTextColor.CGColor;
    text.layer.borderWidth = 1;
    [text addTarget:self action:@selector(resignKeyboard:) forControlEvents:UIControlEventTouchUpOutside];
    [text addTarget:self action:@selector(resignKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    text.delegate = self;
    
    // init toolbar
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, cell.bounds.size.width, 44)];
    toolBar.translucent = NO;
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(clickButtonDone:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpace, btnDone, nil]];
    toolBar.contentMode = UIViewContentModeRight;
    
    toolBar.barTintColor = [PCGraphicManager sharedManager].headerColor;
    btnDone.tintColor = [PCGraphicManager sharedManager].headerTextColor;
    
    [text setInputAccessoryView:toolBar];
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    text.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:text];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(label, text);
    
    NSString *heigth = [NSString stringWithFormat:@"V:|-%.1f@900-[label]-[text(30@900)]-%.1f@900-|", SPACE, SPACE];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heigth options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[text]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView updateConstraints];
    
    // load data
    
    PCField *field = self.fields[indexPath.row];
    
    label.text = field.name;
    text.text = field.value;
    
    text.tag = indexPath.row;
    
    text.keyboardType = UIKeyboardTypeDecimalPad;
    
    return cell;
}

-(UITableViewCell*)cellForPickerWithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellPicker"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    // init viewss
    
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textColor = [PCGraphicManager sharedManager].contentTextColor;
    label.numberOfLines = 0;        
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:label];
    
    // load data
    
    PCField *field = self.fields[indexPath.row];
    
    label.text = field.name;

    NSMutableDictionary *bindings = [[NSMutableDictionary alloc] init];
    NSMutableArray *views = [[NSMutableArray alloc] init];
    
    [bindings setObject:label forKey:@"label"];
    
    for (int i=0; i<field.items.count; i++)
    {
        PCFieldsItem *item = field.items[i];
        
        UIView *container = [[UIView alloc] init];
        
        UISwitch *sw = [[UISwitch alloc] init];
        UILabel *label = [[UILabel alloc] init];
        
        sw.tintColor = [[PCGraphicManager sharedManager] contentTextColor];
        sw.onTintColor = [[PCGraphicManager sharedManager] contentTextColor];
        label.textColor = [PCGraphicManager sharedManager].contentTextColor;
        
        label.numberOfLines = 0;
        
        container.translatesAutoresizingMaskIntoConstraints = NO;
        sw.translatesAutoresizingMaskIntoConstraints = NO;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        
        [container addSubview:sw];
        [container addSubview:label];
        
        label.text = item.name;
        sw.tag = indexPath.row*1000 + i;
        
        if ([item.selected boolValue])
        {
            sw.on = YES;
        }
        
        [sw addTarget:self action:@selector(clickSwitch:) forControlEvents:UIControlEventValueChanged];
        
        NSDictionary *bindingsCOntainer = NSDictionaryOfVariableBindings(sw, label);
        
        NSString *heightSW = [NSString stringWithFormat:@"V:[sw(30.0)]"];
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heightSW options:0 metrics:nil views:bindingsCOntainer]];
        
        [container addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:sw attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
                
        NSString *heightL = [NSString stringWithFormat:@"V:|-[label(>=32.0)]-|"];
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heightL options:0 metrics:nil views:bindingsCOntainer]];
        
        NSString *width = [NSString stringWithFormat:@"H:|[sw(50.0)]-[label]|"];
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:width options:0 metrics:nil views:bindingsCOntainer]];
        
        [views addObject:container];
        
        [cell.contentView addSubview:container];
        
    }
    
    // set contrainsts
    
    NSString *heigth = [NSString stringWithFormat:@"V:|-%.1f-[label]", SPACE];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heigth options:0 metrics:nil views:bindings]];
    
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    
    UIView *view = label;
    
    for (int i=0; i<views.count; i++)
    {
        UIView *view2 = views[i];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view2 attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
        
        [cell.contentView addConstraint:[NSLayoutConstraint
                                       constraintWithItem:view2
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:cell.contentView
                                       attribute:NSLayoutAttributeLeadingMargin
                                       multiplier:1.0f
                                       constant:0]];
        [cell.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:view2
                                         attribute:NSLayoutAttributeTrailing
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:cell.contentView
                                         attribute:NSLayoutAttributeTrailingMargin
                                         multiplier:1.0f
                                         constant:0]];
        view = view2;
    }
    
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.contentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:SPACE]];
    
    [cell.contentView updateConstraints];
    
    return cell;
}

-(UITableViewCell*)cellForSelectWithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellSelect"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    // init views
    
    UILabel *label = [[UILabel alloc] init];
    
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textColor = [PCGraphicManager sharedManager].contentTextColor;
    label.numberOfLines = 0;
    
    UITextField *text = [[UITextField alloc] init];
    text.backgroundColor = [UIColor whiteColor];
    text.delegate = self;
    text.layer.borderColor = [PCGraphicManager sharedManager].contentTextColor.CGColor;
    text.layer.borderWidth = 1;
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    text.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:text];
    
    // init toolbar
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, cell.bounds.size.width, 44)];
    toolBar.translucent = NO;
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(clickButtonDone:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpace, btnDone, nil]];
    toolBar.contentMode = UIViewContentModeRight;
    
    [text setInputAccessoryView:toolBar];
    
    toolBar.barTintColor = [PCGraphicManager sharedManager].headerColor;
    btnDone.tintColor = [PCGraphicManager sharedManager].headerTextColor;
    
    // init picker
    
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 300, 150)];
    picker.dataSource = self;
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    text.inputView = picker;
    picker.tag = indexPath.row;
    
    [picker setBackgroundColor:[PCGraphicManager sharedManager].headerColor];
    
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    text.leftViewMode = UITextFieldViewModeAlways;
    text.leftView = spacerView;
    
    // set contrainstss
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(label, text);
    
    NSString *heigth = [NSString stringWithFormat:@"V:|-%.1f@900-[label]-[text(30)]-%.1f@900-|", SPACE, SPACE];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heigth options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[text]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView updateConstraints];
    
    // load data
    
    PCField *field = self.fields[indexPath.row];
    
    label.text = field.name;    
    
    NSString *str = nil;
    int position = 0;
    for ( int i=0; i<field.items.count; i++ )
    {
        PCFieldsItem *item = field.items[i];
        if ([item.selected boolValue])
        {
            str = item.name;
            position = i;
        }
    }
    
    if (str == nil)
    {
        PCFieldsItem *item = field.items[0];
        str = item.name;
        item.selected = [NSNumber numberWithBool:YES];
    }
    
    text.text = str;
    [picker selectRow:position inComponent:0 animated:YES];
    
    text.tag = indexPath.row;
    
    return cell;
}

-(UITableViewCell*)cellForSendButtonWithIndexPath:(NSIndexPath*) indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellSelect"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];

    // init views
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
    
    button.backgroundColor = [PCGraphicManager sharedManager].headerColor;
    button.tintColor = [PCGraphicManager sharedManager].headerTextColor;
    
    button.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:button];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(button);
    
    NSString *heigth = [NSString stringWithFormat:@"V:|-%.1f@900-[button(40)]-%.1f@900-|", SPACE, SPACE];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heigth options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView updateConstraints];
    
    // load data
    
    [button setTitle:self.confirm forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(clickSendForm:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


#pragma mark - IBActions

-(IBAction)clickButtonDone:(id)sender
{
    if (self.textActual != nil)
    {
//        PCField *filed = self.fields[self.textActual.tag];
//        filed.value = self.textActual.text;
        [self.textActual resignFirstResponder];
//        self.textActual = nil;
    }
    
    if (self.textFieldActual != nil)
    {
//        PCField *filed = self.fields[self.textFieldActual.tag];
//        filed.value = self.textFieldActual.text;
        [self.textFieldActual resignFirstResponder];
//        self.textFieldActual = nil;
    }
}

-(IBAction)resignKeyboard:(id)sender
{
    UITextField *text = (UITextField*)sender;
    
    PCField *field = self.fields[text.tag];
    field.value = text.text;
    
    [text resignFirstResponder];
}

-(IBAction)clickSwitch:(id)sender
{
    UISwitch *sw = (UISwitch*)sender;
    
    PCField *field = self.fields[sw.tag/1000];
    
    PCFieldsItem *fieldItem = field.items[sw.tag%1000];
    
    fieldItem.selected = [NSNumber numberWithBool:[sw isOn]];
}

-(IBAction)clickSendForm:(id)sender
{
    NSString *message = @"";
    
    BOOL valid = YES;
    
    for (PCField *field in self.fields)
    {
        // typ nadpis
        if ([field.type isEqualToString:@"nadpis"])
        {
            NSString *str = [NSString stringWithFormat:@"<h2>%@</h2>", field.name];
            message = [message stringByAppendingString:str];
            
            continue;
        }
        
        // typ test
        if ([field.type isEqualToString:@"text"])
        {
            NSString *str = [NSString stringWithFormat:@"<p><strong>%@: </strong>", field.name];
            if (field.value != nil)
            {
                str = [str stringByAppendingFormat:@"%@</p>", field.value];
            }
            else {
//                str = [str stringByAppendingFormat:@"</p>"];
                valid = NO;
                break;
            }
            
            message = [message stringByAppendingString:str];
            
            continue;
        }
        
        // typ textarea
        if ([field.type isEqualToString:@"textarea"])
        {
            NSString *str = [NSString stringWithFormat:@"<p><strong>%@: </strong>", field.name];
            if (field.value != nil)
            {
                str = [str stringByAppendingFormat:@"%@</p>", field.value];
            }
            else {
//                str = [str stringByAppendingFormat:@"</p>"];
                valid = NO;
                break;
            }
            
            message = [message stringByAppendingString:str];
            
            continue;
        }
        
        // typ number
        if ([field.type isEqualToString:@"number"])
        {
            NSString *str = [NSString stringWithFormat:@"<p><strong>%@: </strong>", field.name];
            if (field.value != nil)
            {
                str = [str stringByAppendingFormat:@"%@</p>", field.value];
            }
            else {
//                str = [str stringByAppendingFormat:@"</p>"];
                valid = NO;
                break;
            }
            
            message = [message stringByAppendingString:str];
            
            continue;
        }
        
        // typ checkbox
        if ([field.type isEqualToString:@"checkbox"])
        {
            NSString *str = [NSString stringWithFormat:@"<p><strong>%@: </strong>", field.name];

            NSString *str2 = @"";
            for (PCFieldsItem *item in field.items)
             {
                 if ([item.selected boolValue])
                 {
                     str2 = [str2 stringByAppendingString:@","];
                     str2 = [str2 stringByAppendingString:item.name];
                 }
             }
            if (str2.length != 0)
            {
                str2 = [str2 substringFromIndex:1];
            }
            
            str = [NSString stringWithFormat:@"%@%@</p>", str, str2];
            
            message = [message stringByAppendingString:str];
            
            continue;
        }
        
        // select
        if ([field.type isEqualToString:@"select"])
        {
            NSString *str = [NSString stringWithFormat:@"<p><strong>%@: </strong>", field.name];
            
            NSString *str2 = @"";
            for (PCFieldsItem *item in field.items)
            {
                if ([item.selected boolValue])
                {
                    str2 = item.name;
                }
            }
            if (str2.length == 0)
            {
                valid = NO;
                break;
            }
            
            
            str = [NSString stringWithFormat:@"%@%@</p>", str, str2];
            
            message = [message stringByAppendingString:str];
            
            continue;
        }
    }
    
    if (!valid)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validace formuláře" message:@"Prosím vyplňte všechny pole" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return;
    }
    
    // send form
    [[PCDataManager sharedManager] sendFormDataWithID:self identificator:self.section message:message success:^{
        // result function
        
        [self clearData];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Formulář odeslán" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
}

#pragma mark UITextView delegate

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.textActual = textView;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
 
        PCField *filed = self.fields[textView.tag];
        filed.value = self.textActual.text;
//        [self.textActual resignFirstResponder];
        self.textActual = nil;
//    }
//    
//    [textView resignFirstResponder];
}


#pragma mark UITextFiel delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.textFieldActual = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    PCField *field = self.fields[textField.tag];
    
    if (![field.type isEqualToString:@"select"])
    {
        field.value = self.textFieldActual.text;
        //    [self.textActual resignFirstResponder];
    }
    self.textFieldActual = nil;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIPicker delegate datasource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    PCField *field = self.fields[pickerView.tag];
    return field.items.count;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    PCField *field = self.fields[pickerView.tag];
    
    PCFieldsItem *item = field.items[row];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:item.name attributes:@{NSForegroundColorAttributeName:[PCGraphicManager sharedManager].headerTextColor}];
    
    return attString;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    PCField *field = self.fields[pickerView.tag];
    
    for (PCFieldsItem *item in field.items)
    {
        item.selected = nil;
    }
    
    PCFieldsItem *item = field.items[row];
    
    item.selected = [NSNumber numberWithBool:YES];
    
    self.textFieldActual.text = item.name;
}

-(void)clearData
{
    for (PCField *field in self.fields)
    {
        // typ nadpis
        if ([field.type isEqualToString:@"nadpis"])
        {
        }
        
        // typ test
        if ([field.type isEqualToString:@"text"])
        {
            field.value = nil;
            
            continue;
        }
        
        // typ textarea
        if ([field.type isEqualToString:@"textarea"])
        {
            field.value = nil;
            
            continue;
        }
        
        // typ number
        if ([field.type isEqualToString:@"number"])
        {
            field.value = nil;
            
            continue;
        }
        
        // typ checkbox
        if ([field.type isEqualToString:@"checkbox"])
        {
            for (PCFieldsItem *item in field.items)
            {
                item.selected = [NSNumber numberWithBool:NO];
            }
            
            continue;
        }
        
        // select
        if ([field.type isEqualToString:@"select"])
        {
            for (PCFieldsItem *item in field.items)
            {
                item.selected = [NSNumber numberWithBool:NO];
            }
            if (field.items.count > 0)
            {
                PCFieldsItem *item = field.items[0];
                item.selected = [NSNumber numberWithBool:YES];
            }
            
            continue;
        }
    }
    
    [self.tableView reloadData];
}

@end
