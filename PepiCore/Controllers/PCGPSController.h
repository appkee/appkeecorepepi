//
//  PCGPSController.h
//  PepiCore
//
//  Created by Radek Zmeskal on 3/5/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCActionController.h"

@interface PCGPSController : UIViewController

/** title of controller */
@property ( strong, atomic) NSString *name;

/** title of controller */
@property ( strong, atomic) NSNumber *latitude;

/** title of controller */
@property ( strong, atomic) NSNumber *longitude;


@end
