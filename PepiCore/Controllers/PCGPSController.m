//
//  PCGPSController.m
//  PepiCore
//
//  Created by Radek Zmeskal on 3/5/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCGPSController.h"
#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "PCGraphicManager.h"


@interface PCGPSController ()

@property ( strong, nonatomic) PCActionController *actionMenu;

@end

@implementation PCGPSController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];
    
    // init views
    
    self.view.backgroundColor = [[PCGraphicManager sharedManager] contentColor];        
    
    UITableViewCell *cellTitle = [[PCTitleManager sharedManager] createTitleRowWithTitle:self.name];
    UIView *viewTitle = cellTitle.contentView;
    
    viewTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:viewTitle];
    
    // set contrainsts
    
    NSDictionary *bindings = [NSDictionary dictionaryWithObjectsAndKeys:viewTitle, @"viewTitle", nil];
    
    NSString *height = [NSString stringWithFormat:@"V:|[viewTitle(>=%.1f)]", [PCTitleManager sharedManager].titleHeight];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:height options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewTitle]|" options:0 metrics:nil views:bindings]];
    
    [self.view updateConstraints];

    
    UILabel *labelInfo = [[UILabel alloc] init];
    labelInfo.numberOfLines = 0;
    
    labelInfo.textColor = [PCGraphicManager sharedManager].contentTextColor;
    labelInfo.font = [UIFont boldSystemFontOfSize:18];
    
    labelInfo.text = @"Pro zobrazení navigace klikněte na jednu následujících z možností. Otevře se Vám příslušná aplikace s nastavenou navigací.";
    
    labelInfo.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIButton *buttonGoogle = [[UIButton alloc] init];
    
    [buttonGoogle setTitle:@"Google maps" forState:UIControlStateNormal];
    buttonGoogle.backgroundColor = [PCGraphicManager sharedManager].headerColor;
    buttonGoogle.tintColor = [PCGraphicManager sharedManager].headerTextColor;
    
    [buttonGoogle addTarget:self action:@selector(clickGoogle:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonGoogle.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIButton *buttonMaps = [[UIButton alloc] init];
    
    [buttonMaps setTitle:@"Maps" forState:UIControlStateNormal];
    buttonMaps.backgroundColor = [PCGraphicManager sharedManager].headerColor;
    buttonMaps.tintColor = [PCGraphicManager sharedManager].headerTextColor;
    
    [buttonMaps addTarget:self action:@selector(clickMaps:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonMaps.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:labelInfo];
    [self.view addSubview:buttonMaps];
    [self.view addSubview:buttonGoogle];
    
    
    bindings = NSDictionaryOfVariableBindings( viewTitle, labelInfo, buttonGoogle, buttonMaps);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[viewTitle]-[labelInfo]-(>=40)-[buttonMaps(40)]-40-[buttonGoogle(40)]-40-|" options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonGoogle]-|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonMaps]-|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[labelInfo]-|" options:0 metrics:nil views:bindings]];
    [self.view updateConstraints];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://maps.apple.com/"]])
    {
        buttonMaps.enabled = true;
        buttonMaps.alpha = 1.0;
    }
    else {
        buttonMaps.enabled = false;
        buttonMaps.alpha = 0.5;
    }
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]])
    {
        buttonGoogle.enabled = true;
        buttonGoogle.alpha = 1.0;
    }
    else {
        buttonGoogle.enabled = false;
        buttonGoogle.alpha = 0.5;
    }
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction

-(IBAction)clickMaps:(id)sender
{
    NSString *url = [NSString stringWithFormat:@"http://maps.apple.com?daddr=%.7f,%.7f&saddr=&dirflg=d", [self.latitude floatValue], [self.longitude floatValue]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(IBAction)clickGoogle:(id)sender
{
    NSString *url = [NSString stringWithFormat:@"comgooglemaps://?saddr=&daddr=%.7f,%.7f&directionsmode=driving", [self.latitude floatValue], [self.longitude floatValue]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

@end
