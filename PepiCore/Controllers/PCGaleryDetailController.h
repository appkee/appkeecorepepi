//
//  GaleryDetailController.h
//  pepi
//
//  Created by Radek Zmeskal on 23/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCActionController.h"
#import "PCImage.h"

@interface PCGaleryDetailController : UIViewController

/** selected index */
@property ( atomic) NSInteger index;
/** list of images */
@property ( strong, atomic) NSArray *images;

@end
