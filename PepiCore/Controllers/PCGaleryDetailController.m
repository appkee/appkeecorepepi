//
//  GaleryDetailController.m
//  pepi
//
//  Created by Radek Zmeskal on 23/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCGaleryDetailController.h"

#import "PCImage.h"
#import "PCNavigationManager.h"
#import "PCGraphicManager.h"

@interface PCGaleryDetailController ()

@property ( strong, nonatomic) PCActionController *actionMenu;

@property ( strong, nonatomic) UIImageView *imageView;

@end

@implementation PCGaleryDetailController

- (void)viewDidLoad {
    [super viewDidLoad];

    // init nav bar
    
    self. actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    // init view
    
    self.view.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    self.imageView = [[UIImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.imageView.clipsToBounds = YES;
    
    self.imageView.userInteractionEnabled = YES;
    
    [self.view addSubview:self.imageView];
    
    // set contrainsts
    
    NSDictionary *bindings = [NSDictionary dictionaryWithObjectsAndKeys:self.imageView, @"image", nil];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|" options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image]|" options:0 metrics:nil views:bindings]];
    
    // load data
    
    PCImage *img = self.images[self.index];
    
    self.imageView.image = [[PCGraphicManager sharedManager] imageForURL:img.url];
    
    // setup gestures
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionRight;
    swipeLeft.numberOfTouchesRequired = 1;
    [self.imageView addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRight.numberOfTouchesRequired = 1;
    [self.imageView addGestureRecognizer:swipeRight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction

-(IBAction)handleSwipeRight:(id)sender
{
    self.index++;
    if (self.images.count == self.index )
    {
        self.index = 0;
    }
    
    [UIView animateWithDuration:2.0 animations:^{
        PCImage *img = self.images[self.index];
    
        self.imageView.image = [[PCGraphicManager sharedManager] imageForURL:img.url];
    }];
}

-(IBAction)handleSwipeLeft:(id)sender
{
    self.index--;
    if (self.index < 0 )
    {
        self.index = self.images.count - 1;
    }
    
    [UIView animateWithDuration:2.0 animations:^{
        PCImage *img = self.images[self.index];
        
        self.imageView.image = [[PCGraphicManager sharedManager] imageForURL:img.url];
    }];
}

@end
