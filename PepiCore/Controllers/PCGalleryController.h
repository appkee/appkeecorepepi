//
//  GaleryController.h
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDMPhotoBrowser.h"
#import "PCActionController.h"

@interface PCGalleryController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, IDMPhotoBrowserDelegate>

/** title of controller */
@property ( strong, atomic) NSString *name;
/** list of images */
@property ( strong, atomic) NSArray *images;

@end
