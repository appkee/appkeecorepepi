//
//  GaleryController.m
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCGalleryController.h"
#import "UIImageView+AFNetworking.h"

#import "PCImage.h"
#import "PCNavigationManager.h"
#import "PCGraphicManager.h"
#import "PCGaleryDetailController.h"
#import "PCTitleManager.h"

@interface PCGalleryController ()

@property ( strong, atomic) PCActionController *actionMenu;
//@property ( strong, atomic) MWPhotoBrowser *browser;

@property ( strong, atomic) NSMutableArray *photos;

@property float cellSize;

@property ( strong, atomic) UICollectionView *collectionView;
@property ( strong, atomic) UICollectionViewFlowLayout *collectionViewLayout;

@end

@implementation PCGalleryController

static NSString * const reuseIdentifierCell = @"cellPhoto";
static NSString * const reuseIdentifierHeader = @"cellHeader";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self  type:MenuActionTypeControllers];
    
    // init views
    
    self.collectionViewLayout = [[UICollectionViewFlowLayout alloc]init];
    self.collectionViewLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsMake( 4, 4, 4, 4);
    self.collectionViewLayout.minimumInteritemSpacing = 1;
    self.collectionViewLayout.minimumLineSpacing = 1;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:self.collectionViewLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    self.collectionView.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    self.cellSize = self.collectionView.frame.size.width/3 - 4;
    
    //
    
    UITableViewCell *cell = [[PCTitleManager sharedManager] createTitleRowWithTitle:self.name];
    UIView *title = cell.contentView;
    
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    title.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.collectionView];
    [self.view addSubview:title];
    
    // set contrainsts
    
    NSDictionary *bindings = [NSDictionary dictionaryWithObjectsAndKeys:title, @"title", self.collectionView, @"collectionView", nil];
    
    NSString *height = [NSString stringWithFormat:@"V:|[title(>=%.1f)][collectionView]-|", [PCTitleManager sharedManager].titleHeight];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:height options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[title]|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:bindings]];
    
    [self.view updateConstraints];
    
    // Register cell classes
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifierCell];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:reuseIdentifierHeader ];
    
    self.photos = [[NSMutableArray alloc] init];
    
    for (PCImage *img in self.images)
    {
        [self.photos addObject:[IDMPhoto photoWithImage:[[PCGraphicManager sharedManager] imageForURL:img.url]]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifierCell forIndexPath:indexPath];
    
    // init view
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 300, 50)];
    image.contentMode = UIViewContentModeScaleAspectFill;
    
    image.translatesAutoresizingMaskIntoConstraints = NO;
    image.clipsToBounds = YES;
    
    [cell.contentView addSubview:image];
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(image);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image]|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    // load data
    
    PCImage *img = self.images[indexPath.row];
    
    image.image = [[PCGraphicManager sharedManager] imageForURL:img.url];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake( self.cellSize, self.cellSize);
}

#pragma mark <UICollectionViewDelegate>

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    GaleryDetailController *viewController = [[GaleryDetailController alloc] init];
//    
//    viewController.images = self.images;
//    viewController.index = indexPath.row;
//    
//    [[self navigationController] pushViewController:viewController animated:YES];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:self.photos];
    browser.delegate = self;
    
    [browser setInitialPageIndex:indexPath.row];
    
    // Set options
    browser.displayActionButton = NO;
    browser.displayArrowButton = YES;
    browser.displayCounterLabel = NO;
//    browser.useWhiteBackgroundColor = YES;
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
    UIImage *imageDone = [UIImage imageNamed:@"IDMPhotoBrowser_close" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    UIImage *imageLeft = [UIImage imageNamed:@"IDMPhotoBrowser_arrowLeft" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    UIImage *imageRight = [UIImage imageNamed:@"IDMPhotoBrowser_arrowRight" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    browser.doneButtonImage         = imageDone;
    browser.leftArrowImage          = imageLeft;
    browser.rightArrowImage         = imageRight;
    
    
    browser.view.tintColor          = [UIColor whiteColor];
    browser.progressTintColor       = [UIColor whiteColor];
    browser.trackTintColor          = [UIColor colorWithWhite:0.8 alpha:1];
    
    // Present
    [self presentViewController:browser animated:YES completion:nil];

}

#pragma mark - IDMPhotoBrowser Delegate

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)pageIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:pageIndex];
//    NSLog(@"Did show photoBrowser with photo index: %d, photo caption: %@", pageIndex, photo.caption);
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)pageIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:pageIndex];
//    NSLog(@"Will dismiss photoBrowser with photo index: %d, photo caption: %@", pageIndex, photo.caption);
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissAtPageIndex:(NSUInteger)pageIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:pageIndex];
//    NSLog(@"Did dismiss photoBrowser with photo index: %d, photo caption: %@", pageIndex, photo.caption);
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissActionSheetWithButtonIndex:(NSUInteger)buttonIndex photoIndex:(NSUInteger)photoIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:photoIndex];
//    NSLog(@"Did dismiss actionSheet with photo index: %d, photo caption: %@", photoIndex, photo.caption);
    
//    NSString *title = [NSString stringWithFormat:@"Option %d", buttonIndex+1];
    //    [UIAlertView showAlertViewWithTitle:title];
}


/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/



@end
