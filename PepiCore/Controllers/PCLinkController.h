//
//  PageController.h
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCActionController.h"

@interface PCLinkController : UIViewController<UIWebViewDelegate>

/** title of controller */
@property (strong, atomic) NSString *name;
/** link to load */
@property (strong, atomic) NSString *link;

-(void)loadLink;

@end
