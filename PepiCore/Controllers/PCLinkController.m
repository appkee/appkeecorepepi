//
//  PageController.m
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCLinkController.h"
#import "PCSection.h"

#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "PCGraphicManager.h"
#import "PCDefaults.h"

#import "MBProgressHUD.h"

@interface PCLinkController ()

@property BOOL loading;

// action menu
@property PCActionController *actionMenu;

// web view
@property ( strong, nonatomic) UIWebView *webView;

@property ( strong, nonatomic) MBProgressHUD *progressHUD;

@end

@implementation PCLinkController

- (void)viewDidLoad {
    [super viewDidLoad];

    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];

    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllersLink];
    
    self.view.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    // Do any additional setup after loading the view.
    
    UITableViewCell *cellTitle = [[PCTitleManager sharedManager] createTitleRowWithTitle:self.name];
    UIView *viewTitle = cellTitle.contentView;
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    self.webView.delegate = self;
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    viewTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.webView];
    [self.view addSubview:viewTitle];
    
    // set contrainsts
    
    NSDictionary *bindings = [NSDictionary dictionaryWithObjectsAndKeys:self.webView, @"webView", viewTitle, @"viewTitle", nil];
    
    NSString *height = [NSString stringWithFormat:@"V:|[viewTitle(>=%.1f)][webView]-|", [PCTitleManager sharedManager].titleHeight];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:height options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewTitle]|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView]|" options:0 metrics:nil views:bindings]];
    
    [self.view updateConstraints];
    
    [self loadLink];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIWebView delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.progressHUD hide:YES];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.progressHUD hide:YES];
    
    if ([error code] == NSURLErrorNotConnectedToInternet || [error code] == NSURLErrorNetworkConnectionLost)
    {
        UIAlertView *alert = nil;

        if ([[PCDefaults sharedInstance] getTester])
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Chyba načtení odkazu" message:@"Zkontrolujte připojení k internetu" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        }
        else {
            alert = [[UIAlertView alloc] initWithTitle:@"Chyba načtení odkazu" message:@"Zkontrolujte addresu nebo připojení k internetu" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        }
        [alert show];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - actions

-(void)loadLink
{
    // load view
    
    self.loading = true;
    
    [self.webView stopLoading];
    
    if (self.link != nil)
    {
        self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:self.progressHUD];
        self.progressHUD.color = [PCGraphicManager sharedManager].headerColor;
        
        [self.progressHUD show:YES];
        
        NSURL *url = [NSURL URLWithString:self.link];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        
        [self.webView loadRequest:urlRequest];
    }
    else {
        self.webView.hidden = YES;
    }
}

@end
