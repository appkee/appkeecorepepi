//
//  PCNotificationController.h
//  PepiCore
//
//  Created by Radek Zmeskal on 4/10/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCNotificationController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDetail;

@property NSString *name;
@property NSString *detail;

@end
