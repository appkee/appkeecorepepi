//
//  PCNotificationController.m
//  PepiCore
//
//  Created by Radek Zmeskal on 4/10/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCNotificationController.h"

@interface PCNotificationController ()

@end

@implementation PCNotificationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.labelTitle.text = self.name;
    self.labelDetail.text = self.detail;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)clickOK:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
