//
//  NotificationController.h
//  pepi
//
//  Created by Radek Zmeskal on 1/17/16.
//  Copyright © 2016 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCNotificationSettingsController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageSettings;
@property (weak, nonatomic) IBOutlet UIImageView *imageNotifi;
@property (weak, nonatomic) IBOutlet UIImageView *imagelSwitch;

@end
