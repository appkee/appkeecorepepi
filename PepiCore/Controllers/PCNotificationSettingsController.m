//
//  NotificationController.m
//  pepi
//
//  Created by Radek Zmeskal on 1/17/16.
//  Copyright © 2016 Tripon. All rights reserved.
//

#import "PCNotificationSettingsController.h"

@interface PCNotificationSettingsController ()

@end

@implementation PCNotificationSettingsController

- (void)viewDidLoad {
    
    self.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
    UIImage *imageSettings = [UIImage imageNamed:@"PC_info_settings" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    UIImage *imageNotifi = [UIImage imageNamed:@"PC_info_notifi" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    UIImage *imagelSwitch = [UIImage imageNamed:@"PC_info_switch" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    
    self.imageSettings.image = imageSettings;
    self.imageNotifi.image = imageNotifi;
    self.imagelSwitch.image = imagelSwitch;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark

-(IBAction)clickSettings:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)clickCancel:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
