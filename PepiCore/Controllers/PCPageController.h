//
//  PageController.h
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCActionController.h"
#import "IDMPhotoBrowser.h"
#import "YTPlayerView.h"

@interface PCPageController : UITableViewController<UIWebViewDelegate, IDMPhotoBrowserDelegate, YTPlayerViewDelegate>

/** identificator of controller */
@property ( strong, atomic) NSNumber *identificator;

/** title of controller */
@property ( strong, atomic) NSString *name;
/** array of images */
@property ( strong, atomic) NSArray *images;
/** array of texts **/
@property ( strong, atomic) NSArray *texts;

@property ( strong, atomic) NSArray *videos;

@end
