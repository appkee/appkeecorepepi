//
//  PageController.m
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCPageController.h"
#import "UIImageView+AFNetworking.h"

#import "PCImage.h"
#import "PCText.h"
#import "PCGraphicManager.h"
#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "HexColors.h"
#import "PCVideo.h"
#import "PCVideoCell.h"
#import "PCDefaults.h"

@interface PCPageController ()

@property ( strong, nonatomic) PCActionController *actionMenu;

@property ( strong, nonatomic) NSArray *rows;

//@property ( strong, nonatomic) MWPhoto *selectedImage;

@property ( strong, atomic) NSMutableArray *photos;

@end

@implementation PCPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:NULL];
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self  type:MenuActionTypeControllers];
    
    // init views
    
    self.tableView.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    self.tableView.estimatedRowHeight = 70;
    self.tableView.estimatedSectionHeaderHeight = [[PCTitleManager sharedManager] titleHeight];
    
    self.tableView.allowsSelection = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    // init data
 
    NSArray *array = [self.images arrayByAddingObjectsFromArray:self.texts];
    
    array = [array arrayByAddingObjectsFromArray:self.videos];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    
    array = [array sortedArrayUsingDescriptors:@[sortDescriptor]];
        
    self.rows = array;
    
    self.photos = [[NSMutableArray alloc] init];
    
    for (PCImage *img in self.images)
    {
        [self.photos addObject:[IDMPhoto photoWithImage:[[PCGraphicManager sharedManager] imageForURL:img.url]]];
    }
    
//    UINib *cellNib = [UINib nibWithNibName:@"PCVideoCell" bundle:nil];
//    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"cellVideo"];
}

//-(void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    
//    UIEdgeInsets insets = UIEdgeInsetsMake(5, 5, 5, 5);
//    self.navigationController.tabBarItem.imageInsets = insets;
//    
//
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PCContent *content = self.rows[indexPath.row];
    
    if ([content isKindOfClass:[PCVideo class]])
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellVideo"];
        
        // init views
        
        YTPlayerView *playerView = [[YTPlayerView alloc] init];
        [playerView setTag:-1];
        
        playerView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [cell.contentView addSubview:playerView];
        
        NSDictionary *bindings = NSDictionaryOfVariableBindings(playerView);
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[playerView(180)]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[playerView]-|" options:0 metrics:nil views:bindings]];
        
        cell.backgroundColor = [UIColor clearColor];
        
        playerView.delegate = self;
        
        PCVideo *video = (PCVideo*)content;
         
        NSDictionary *playerVars = @{
                                     @"controls" : @1,
                                     @"playsinline" : @1,
                                     @"autohide" : @1,
                                     @"showinfo" : @0,
                                     @"modestbranding" : @1
                                     };
        
        
        [playerView loadWithVideoId:video.url playerVars:playerVars];
        
        return cell;
    }
    
    if ([content isKindOfClass:[PCImage class]])
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellImage"];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        // init views
        
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 300, 50)];
        image.contentMode = UIViewContentModeScaleAspectFit;
        image.clipsToBounds = YES;
        image.translatesAutoresizingMaskIntoConstraints = NO;
        image.tag = -1;
        
        UIButton *button = [[UIButton alloc] init];
        button.tag = indexPath.row;
        
        // load data
        
        UIImage *img = [[PCGraphicManager sharedManager] imageForURL:((PCImage*)content).url];
        
        image.image = img;
        
        [button addTarget:self action:@selector(clickImage:) forControlEvents:UIControlEventTouchDown];
        
        image.translatesAutoresizingMaskIntoConstraints = NO;
        button.translatesAutoresizingMaskIntoConstraints = NO;
        
        
        [cell.contentView addSubview:image];
        [cell.contentView addSubview:button];
        
        // calculate height of image
        
        float scale = 0;
        
        if (img != nil)
        {
            scale = img.size.height/img.size.width;
        }
        
        float height = scale * cell.contentView.frame.size.width;
        
        // set contrainsts
        
        NSDictionary *bindings = NSDictionaryOfVariableBindings(image, button);
        
        NSString *str = [NSString stringWithFormat:@"V:|-[image(%.1f@900)]-|", height];
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:str options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[image]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[button]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] updateConstraints];
        
        return cell;
    }
    
    if ([content isKindOfClass:[PCText class]])
    {
         UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellText"];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        // init views
        
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 10, 300, 50)];
        
        webView.translatesAutoresizingMaskIntoConstraints = NO;
        webView.scrollView.scrollEnabled = NO;
        webView.scrollView.bounces = NO;
        
        webView.backgroundColor = [UIColor clearColor];
        webView.opaque = NO;
        webView.scalesPageToFit = NO;
        webView.dataDetectorTypes = UIDataDetectorTypePhoneNumber;
        
        webView.delegate = self;
        
        [cell.contentView addSubview:webView];
        
        // set contrainsts
        
        NSDictionary *bindings = NSDictionaryOfVariableBindings(webView);
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[webView]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[webView]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] updateConstraints];
        
        // load data
        
        NSString *s;
        NSString *css = [[PCDefaults sharedInstance] getCss];
        if (css == nil)
        {
            s = [NSString stringWithFormat:@"<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px 10px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;}  </style>", [PCGraphicManager sharedManager].contentTextColorHEX ];
        }
        else {
            s = [NSString stringWithFormat:@"<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px 10px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;} %@ </style>", [PCGraphicManager sharedManager].contentTextColorHEX, css ];
        }
        
        NSString *str = [NSString stringWithFormat:@"<html> <body style=\"background-color:transparent; color:%@ \"> %@ %@  </body></html>", [PCGraphicManager sharedManager].contentTextColorHEX , s, ((PCText*)content).content];
        
        [webView loadHTMLString:str baseURL:[NSURL URLWithString:@""]];
        
        [webView  sizeToFit];
        
        cell.backgroundColor = [UIColor clearColor];
        
        webView.tag = indexPath.row;
        
        return cell;
    }
    
    UITableViewCell *cell2 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellPage"];
    
    return cell2;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    PCContent *content = self.rows[indexPath.row];
    
    if ([content isKindOfClass:[PCText class]])
    {
        PCText *text = (PCText*)content;
        if (text.height != nil)
        {
            return 8+[text.height floatValue]+8;
        }
    }
    
    return UITableViewAutomaticDimension;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [[PCTitleManager sharedManager] createTitleRowWithTitle:self.name];        
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UIWebView delegate

- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    PCText *text = self.rows[aWebView.tag];

    if (text.height != nil)
    {
        return;
    }
    
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    aWebView.frame = frame;
    
    text.height = [NSNumber numberWithFloat:frame.size.height];
    
//    [UIView setAnimationsEnabled:NO];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
//    [UIView setAnimationsEnabled:YES];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    
#pragma TODO test emailu
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

#pragma mark - IBActions

-(IBAction)clickImage:(id)sender
{
    PCImage *img = (PCImage*)self.rows[[sender tag]];
    
    NSInteger row = [img.position integerValue];
    
//    if (row >= self.images.count)
//    {
//        row = self.images.count - 1;
//    }
//    else {
//        
//    }
    
    NSInteger index = 0;
    
    for (int i=0; i<self.photos.count; i++)
    {
        PCImage *img = self.images[i];
        if (row == [img.position integerValue])
        {
            index = i;
            break;
        }
    }
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:self.photos];
    browser.delegate = self;
    
    [browser setInitialPageIndex:index];
    
    // Set options
    browser.displayActionButton = NO;
    browser.displayArrowButton = YES;
    browser.displayCounterLabel = NO;
//    browser.useWhiteBackgroundColor = YES;
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
    UIImage *imageDone = [UIImage imageNamed:@"IDMPhotoBrowser_close" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    UIImage *imageLeft = [UIImage imageNamed:@"IDMPhotoBrowser_arrowLeft" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    UIImage *imageRight = [UIImage imageNamed:@"IDMPhotoBrowser_arrowRight" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    browser.doneButtonImage         = imageDone;    
    browser.leftArrowImage          = imageLeft;
    browser.rightArrowImage         = imageRight;

    
    browser.view.tintColor          = [UIColor whiteColor];
    browser.progressTintColor       = [UIColor whiteColor];
    browser.trackTintColor          = [UIColor colorWithWhite:0.8 alpha:1];

    
    
    // Present
    [self presentViewController:browser animated:YES completion:nil];
}

#pragma mark - IDMPhotoBrowser Delegate

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didShowPhotoAtIndex:(NSUInteger)pageIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:pageIndex];
//    NSLog(@"Did show photoBrowser with photo index: %d, photo caption: %@", pageIndex, photo.caption);
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)pageIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:pageIndex];
//    NSLog(@"Will dismiss photoBrowser with photo index: %d, photo caption: %@", pageIndex, photo.caption);
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissAtPageIndex:(NSUInteger)pageIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:pageIndex];
//    NSLog(@"Did dismiss photoBrowser with photo index: %d, photo caption: %@", pageIndex, photo.caption);
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissActionSheetWithButtonIndex:(NSUInteger)buttonIndex photoIndex:(NSUInteger)photoIndex
{
    id <IDMPhoto> photo = [photoBrowser photoAtIndex:photoIndex];
//    NSLog(@"Did dismiss actionSheet with photo index: %d, photo caption: %@", photoIndex, photo.caption);
    
//    NSString *title = [NSString stringWithFormat:@"Option %d", buttonIndex+1];
//    [UIAlertView showAlertViewWithTitle:title];
}

-(void)playerView:(YTPlayerView *)playerView didPlayTime:(float)playTime
{
    NSLog(@"");
}

-(void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state
{
    NSLog(@"");
}

-(void)playerView:(YTPlayerView *)playerView didChangeToQuality:(YTPlaybackQuality)quality
{
    NSLog(@"");
}

-(void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error
{
    NSLog(@"");
}

@end
