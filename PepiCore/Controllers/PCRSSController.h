//
//  RSSController.h
//  pepi
//
//  Created by Radek Zmeskal on 16/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCActionController.h"
#import "MBProgressHUD.h"

@interface PCRSSController : UITableViewController<NSXMLParserDelegate, UIWebViewDelegate>

/** title of controller */
@property ( strong, atomic) NSString *name;
/** link to rss feed */
@property ( strong, atomic) NSString *rssFeed;

-(void)loadLink;

@end
