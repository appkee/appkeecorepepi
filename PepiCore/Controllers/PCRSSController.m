//
//  RSSController.m
//  pepi
//
//  Created by Radek Zmeskal on 16/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCRSSController.h"
#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "PCRSSItem.h"
#import "PCGraphicManager.h"
#import "PCDefaults.h"

@interface PCRSSController ()

@property ( strong, nonatomic) PCActionController *actionMenu;

@property ( strong, nonatomic) NSXMLParser *rssParser;
@property ( strong, nonatomic) NSMutableArray *stories;

@property ( strong, nonatomic) NSString *currentElement;
@property ( strong, nonatomic) NSMutableString * currentTitle, * currentDate, * currentSummary, * currentLink;

//@property ( strong, nonatomic) MBProgressHUD *progressHUD;


@end

@implementation PCRSSController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllersLink];
    
    // init views
    
    self.tableView.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 90;
    self.tableView.estimatedSectionHeaderHeight = [[PCTitleManager sharedManager] titleHeight];
    
    self.tableView.allowsSelection = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (self.rssFeed != nil)
    {
        if ([self.stories count] == 0)
        {
            // load stories
            MBProgressHUD *HUD =  [MBProgressHUD showHUDAddedTo: self.view animated: YES];
            HUD.removeFromSuperViewOnHide = true;
            
            [self parseXMLFileAtURL:self.rssFeed];
        }
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    if ([self.stories count] == 0)
//    {
//        [self.progressHUD show:YES];
//        
//        [self parseXMLFileAtURL:self.rssFeed];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.stories.count * 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PCRSSItem *story = self.stories[indexPath.row/2];
 
    UITableViewCell *cell;
    
    // test title of cell
    if (indexPath.row %2 == 0)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellTitle"];
        
        [cell.contentView setNeedsLayout];
        [cell.contentView layoutIfNeeded];
        
        // init views
        
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 40)];
        labelTitle.numberOfLines = 0;
        labelTitle.font = [UIFont boldSystemFontOfSize:20];
        labelTitle.textColor = [PCGraphicManager sharedManager].contentTextColor;
        
        labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
        
        [cell.contentView addSubview:labelTitle];
        
        // set contrainsts
        
        NSDictionary *bindings = NSDictionaryOfVariableBindings( labelTitle);
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelTitle]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[labelTitle]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] updateConstraints];
        
        // load data
        
        labelTitle.text = story.title;
        
        UIButton *buttonTitle = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 300, 40)];
        buttonTitle.translatesAutoresizingMaskIntoConstraints = NO;
        buttonTitle.backgroundColor = [UIColor clearColor];
        buttonTitle.tag = indexPath.row/2;
        
        [cell.contentView addSubview:buttonTitle];
        
        // set contrainsts
        
        bindings = NSDictionaryOfVariableBindings( buttonTitle);
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[buttonTitle]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonTitle]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] updateConstraints];
        
        [buttonTitle addTarget:self action:@selector(clickTitle:) forControlEvents:UIControlEventTouchDown];
    }
    else {
         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellMenu"];
        
        [cell.contentView setNeedsLayout];
        [cell.contentView layoutIfNeeded];
        
        // init views
        
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 10, 300, 60)];
        
        webView.translatesAutoresizingMaskIntoConstraints = NO;
        webView.scrollView.scrollEnabled = NO;
        webView.scrollView.bounces = NO;
//        webView.scalesPageToFit = true;
        
        webView.delegate = self;
        
        webView.backgroundColor = [UIColor clearColor];
        webView.opaque = NO;
        
        [cell.contentView addSubview:webView];
        
        // set contrainsts
        
        NSDictionary *bindings = NSDictionaryOfVariableBindings( webView);
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[webView]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[webView]-|" options:0 metrics:nil views:bindings]];
        
        [[cell contentView] updateConstraints];
        
        // load data
        
        if (story.summary != nil)
        {
            NSString *s;
            NSString *css = [[PCDefaults sharedInstance] getCss];
            if (css == nil)
            {
                s = [NSString stringWithFormat:@"<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px 10px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;}  </style>", [PCGraphicManager sharedManager].contentTextColorHEX ];
            }
            else {
                s = [NSString stringWithFormat:@"<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px 10px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;} %@ </style>", css, [PCGraphicManager sharedManager].contentTextColorHEX ];
            }
            
            NSString *str = [NSString stringWithFormat:@"<html> <head><style>img{max-width:100%%;height:auto !important;width:auto !important;};</style></head> <body style=\"background-color:transparent; color:%@ \"> %@ %@  </body></html>", [PCGraphicManager sharedManager].contentTextColorHEX , s, story.summary];
            
            [webView loadHTMLString:str baseURL:[NSURL URLWithString:@""]];
        }
        
        webView.tag = indexPath.row/2;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 != 0)
    {
        PCRSSItem *story = [self.stories objectAtIndex: indexPath.row/2];
        
        if (story.height != nil)
        {
            return 8+[story.height floatValue]+8;
        }

    }
    
    return UITableViewAutomaticDimension;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [[PCTitleManager sharedManager] createTitleRowWithTitle:self.name];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - oarser XML delegate

- (void)parseXMLFileAtURL:(NSString *)URL
{
    self.stories = [[NSMutableArray alloc] init];
    
    //you must then convert the path to a proper NSURL or it won't work
    NSURL *xmlURL = [NSURL URLWithString:URL];
    
    // here, for some reason you have to use NSClassFromString when trying to alloc NSXMLParser, otherwise you will get an object not found error
    // this may be necessary only for the toolchain
    self.rssParser = [[NSXMLParser alloc] initWithContentsOfURL:xmlURL];
    
    // Set self as the delegate of the parser so that it will receive the parser delegate methods callbacks.
    [self.rssParser setDelegate:self];
    
    // Depending on the XML document you're parsing, you may want to enable these features of NSXMLParser.
    [self.rssParser setShouldProcessNamespaces:NO];
    [self.rssParser setShouldReportNamespacePrefixes:NO];
    [self.rssParser setShouldResolveExternalEntities:NO];
    
    [self.rssParser parse];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    NSLog(@"found file and started parsing");
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSString * errorString = [NSString stringWithFormat:@"Nepodařilo se načíst RSS feed ze stránky (Kód chyby %i )", [parseError code]];
    NSLog(@"error parsing XML: %@", errorString);
    
    UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:@"Chyba načtení" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    //NSLog(@"found this element: %@", elementName);
    self.currentElement = [elementName copy];
    
    if ([elementName isEqualToString:@"item"]) {
        // clear out our story item caches...

        self.currentTitle = [[NSMutableString alloc] init];
        self.currentDate = [[NSMutableString alloc] init];
        self.currentSummary = [[NSMutableString alloc] init];
        self.currentLink = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    //NSLog(@"ended element: %@", elementName);
    if ([elementName isEqualToString:@"item"]) {
        // save values to an item, then store that item into the array...
        
        PCRSSItem *item = [[PCRSSItem alloc] initWithTitle:self.currentTitle date:self.currentDate sumary:self.currentSummary link:self.currentLink];
        
        [self.stories addObject:item];
        NSLog(@"adding story: %@", self.currentTitle);
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    //NSLog(@"found characters: %@", string);
    // save the characters for the current item...
    if ([self.currentElement isEqualToString:@"title"])
    {
        [self.currentTitle appendString:string];
    } else
        if ([self.currentElement isEqualToString:@"link"])
        {
            [self.currentLink appendString:string];
        } else
            if ([self.currentElement isEqualToString:@"description"])
            {
                [self.currentSummary appendString:string];
            } else
                if ([self.currentElement isEqualToString:@"pubDate"])
                {
                    [self.currentDate appendString:string];
                }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    [self.tableView reloadData];
    
    [MBProgressHUD hideHUDForView: self.view animated: YES];
}

#pragma mark - UIWebView delegate

- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    PCRSSItem *item = self.stories[aWebView.tag];
    
    if (item.height != nil)
    {
        return;
    }
    
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    aWebView.frame = frame;
    
    item.height = [NSNumber numberWithFloat:frame.size.height];
    
    //    [UIView setAnimationsEnabled:NO];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    //    [UIView setAnimationsEnabled:YES];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

#pragma mark - actions

-(void)loadLink
{
    if (self.rssFeed != nil)
    {
        // load stories
//        [self.progressHUD show:YES];
        [MBProgressHUD hideHUDForView: self.view animated: YES];
        
        [self parseXMLFileAtURL:self.rssFeed];
    }
}

-(IBAction)clickTitle:(id)sender
{
    NSLog(@"touch title");
    NSInteger tag = [sender tag];
    PCRSSItem *story = self.stories[tag];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Otevřít stránku s detailem?"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ano"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:story.link]];
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"NE"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    if (story.link != nil)
    {
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


@end
