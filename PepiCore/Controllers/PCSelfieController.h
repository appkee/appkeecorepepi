//
//  PCSelfieController.h
//  PepiCore
//
//  Created by Radek Zmeskal on 3/6/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCSelfieController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property NSString *name;

@property UIImage *logo;

@end
