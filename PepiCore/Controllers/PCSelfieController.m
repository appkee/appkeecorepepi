//
//  PCSelfieController.m
//  PepiCore
//
//  Created by Radek Zmeskal on 3/6/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCSelfieController.h"
#import "PCGraphicManager.h"
#import "PCActionController.h"
#import "PCNavigationManager.h"
#import "UIImage+AFNetworking.h"

//UIActivityViewController
//UIPopoverPresentationController

@interface PCSelfieController ()

@property UIImageView *imageView;

@property UIImage *imageOverview;

@property UIButton *buttonTake;

@property ( strong, nonatomic) PCActionController *actionMenu;

@end

@implementation PCSelfieController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    //    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(clickDone:)];
    
    self.navigationItem.rightBarButtonItem = barButtonDone;

    // init views
    ;
    self.view.backgroundColor = [UIColor blackColor];
    
    self.imageView = [[UIImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.buttonTake = [[UIButton alloc] init];
//    [self.buttonTake setBackgroundColor:[PCGraphicManager sharedManager].contentColor];
//    [self.buttonTake setTitleColor:[PCGraphicManager sharedManager].contentTextColor forState:UIControlStateNormal];
    
//    [buttonTake setTitle:@"Vyfotit" forState:UIControlStateNormal];
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
    
    UIImage *imageTake = [UIImage imageNamed:@"photo_take" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    [self.buttonTake setImage:imageTake forState:UIControlStateNormal];
    
    [self.buttonTake addTarget:self action:@selector(clickTake:) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.buttonTake.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.buttonTake];
    
    UIImageView *img = self.imageView;
    
    NSDictionary *bindings = [[NSDictionary alloc] initWithObjectsAndKeys: img, @"img", self.buttonTake, @"buttonTake", nil];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[img]-16-[buttonTake]" options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[buttonTake(50)]-20-|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[img]-|" options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonTake]-|" options:0 metrics:nil views:bindings]];
    
    
//    [self.view updateConstraints];
    
    
    UIGraphicsBeginImageContextWithOptions(self.imageOverview.size, NO, 0);
    UIBezierPath* p =
    [UIBezierPath bezierPathWithRect:CGRectMake(0,0,self.imageOverview.size.width,self.imageOverview.size.height)];
    [[UIColor blackColor] setFill];
    [p fill];
    UIImage* im = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self showImage:im];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)showImage:(UIImage*) image
{
    CGRect rect = CGRectMake( 0, 0, image.size.width, image.size.height);
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
    
    UIImage* temp = UIGraphicsGetImageFromCurrentImageContext();
    [[UIColor blackColor] set];
    UIRectFill(CGRectMake(0.0, 0.0, temp.size.width, temp.size.height));
    [temp drawInRect:rect];
    
    CGRect rectImage = CGRectMake( 60, 60, image.size.width - 120, image.size.height - 120);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rectImage byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(100, 100)];
    [path addClip];
    
    [image drawInRect:rect];
    NSLog(@"%.1f %.1f", self.logo.size.width, self.logo.size.height);
    
    float scale = (image.size.width/10.0)/self.logo.size.height;
    
    CGFloat width = self.logo.size.width*scale;
    CGFloat heigth = self.logo.size.height*scale;
    
    if (width > (image.size.width*0.9))
    {
        scale = (image.size.height*0.9)/self.logo.size.width;
        
        width = self.logo.size.width*scale;
        heigth = self.logo.size.height*scale;
    }
    
    CGFloat x = image.size.width*(9.5/10.0) - width;
    CGFloat y = image.size.height*(9.5/10.0) - heigth;
    CGRect rectLogo = CGRectMake( x, y, width, heigth);
    
    [self.logo drawInRect:rectLogo];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.imageView.image = newImage;
    
}

#pragma mark - IBActions

-(IBAction)clickTake:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [self showImage:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
        
        UIImage *imageReload = [UIImage imageNamed:@"photo_reload" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
        
        [self.buttonTake setImage:imageReload forState:UIControlStateNormal];
        
        
        UIImage * image = self.imageView.image;
        
        NSArray * shareItems = @[ image];
        
        UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
        
        
        [self presentViewController:avc animated:YES completion:nil];
    }];
}

-(IBAction)clickDone:(id)sender
{
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}



@end
