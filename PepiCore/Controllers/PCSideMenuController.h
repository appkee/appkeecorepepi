//
//  SideMenuController.h
//  pepi
//
//  Created by Radek Zmeskal on 13/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PCAppDescription.h"

@interface PCSideMenuController : UIViewController<UITableViewDataSource, UITableViewDelegate>

/** llist of section */
@property NSArray *sections;

/** app description  */
@property (strong, nonatomic) PCAppDescription *appDescription;

@end
