//
//  SideMenuController.m
//  pepi
//
//  Created by Radek Zmeskal on 13/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCSideMenuController.h"
#import "UIImageView+AFNetworking.h"
#import "PCGraphicManager.h"
#import "SWRevealViewController.h"
#import "PCSection.h"
#import "PCDefaults.h"

@interface PCSideMenuController ()

@property ( strong, nonatomic) UITableView *tableView;

@property ( strong, nonatomic) NSIndexPath *indexPath;

@property UIView *infoView;

@end

@implementation PCSideMenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init info view
    
    self.infoView = [[PCDefaults sharedInstance] getInfoController].view;
    
    if (self.infoView != nil)
    {
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(curlHideView:)];
        [self.infoView addGestureRecognizer:singleFingerTap];
    }
    
    // init views
    
    self.tableView = [[UITableView alloc] init];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.view.backgroundColor = [PCGraphicManager sharedManager].menuColor;
    
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 70;

    self.tableView.separatorColor = [PCGraphicManager sharedManager].menuTextColor;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    if (self.sections.count > 0)
    {
        self.indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView selectRowAtIndexPath:self.indexPath animated:YES  scrollPosition:UITableViewScrollPositionTop];
    }
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 50)];
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.numberOfLines = 0;
    label.textColor = [[PCGraphicManager sharedManager] menuTextColor];
    label.font = [UIFont boldSystemFontOfSize:20.0 ];
    
    [self.view addSubview:label];
    [self.view addSubview:self.tableView];
    
    // set contrainsts
    
    NSDictionary *bindings = [NSDictionary dictionaryWithObjectsAndKeys:self.tableView, @"tableView", label, @"label", nil];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-26-[label(>=30.0)]-[tableView]-|" options:0 metrics:nil views:bindings]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:bindings]];
    
    // load data
    
    label.text = self.appDescription.name;
    
    label.backgroundColor = [PCGraphicManager sharedManager].menuColor;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.infoView == nil)
    {
        return self.sections.count;
    }
    
    return self.sections.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellMenu"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [PCGraphicManager sharedManager].menuTextColor;
    cell.selectedBackgroundView = v;

    // init views
    
    UIImageView *image = [[UIImageView alloc] init];
    image.contentMode = UIViewContentModeScaleAspectFit;
    image.tintColor = [PCGraphicManager sharedManager].menuTextColor;
    
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.textColor = [PCGraphicManager sharedManager].menuTextColor;
    label.highlightedTextColor = [PCGraphicManager sharedManager].menuColor;
    
    

    label.translatesAutoresizingMaskIntoConstraints = NO;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:image];
    [cell.contentView addSubview:label];
    
    // set constraints
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(image, label);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[image(20)]" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[label(>=40@900)]-|" options:0 metrics:nil views:bindings]];
  
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[image(50)]-[label]-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    // load data
    
    if ((indexPath.row >= self.sections.count) && (self.infoView != nil))
    {
        label.text = @"O aplikaci";
        
        image.image = [UIImage imageNamed:@"pepi_info"];
    }
    else {
        PCSection *section = self.sections[indexPath.row];
        
        image.image = [[PCGraphicManager sharedManager] imageForURL:section.icon];
        
        label.text = section.name;
    }
    
    image.image = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    UIImage *img = [image.image copy];
//    img =

//    image.highlightedImage = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
//    cell.selectedBackgroundView.backgroundColor = [GraphicManager sharedManager].menuTextColor;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.row == self.sections.count) && (self.infoView != nil))
    {
        [self performSelector:@selector(curlShowView2:) withObject:nil];
        return;
    }
    else {
        self.indexPath = indexPath;
    }
    
    PCSection *section = self.sections[indexPath.row];
    
    if (!section.navigationController) {
        [section createNavigationController];
    }
        
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
    UIImage *imageDots = [UIImage imageNamed:@"reveal-icon" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:imageDots style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    section.viewController.navigationItem.leftBarButtonItem = revealButtonItem;
    
    [self.revealViewController pushFrontViewController:section.navigationController animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Curl functionality

- (IBAction)curlShowView2:(id)sender
{
    
    [UIView animateWithDuration:1.0
                     animations:^{
                         CATransition *animation = [CATransition animation];
                         [animation setDelegate:self];
                         [animation setDuration:0.7];
                         [animation setTimingFunction:[CAMediaTimingFunction functionWithName:@"default"]];
                         animation.type = @"pageCurl";
                         animation.fillMode = kCAFillModeForwards;
                         animation.endProgress = 0.6;
                         [animation setRemovedOnCompletion:NO];
                         [self.revealViewController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
                         [self.revealViewController.view addSubview:self.infoView];
                         ;}
     ];
}

- (IBAction)curlHideView:(id)sender {
    [UIView animateWithDuration:1.0
                     animations:^{
                         CATransition *animation = [CATransition animation];
                         [animation setDelegate:self];
                         [animation setDuration:0.7];
                         [animation setTimingFunction:[CAMediaTimingFunction functionWithName:@"default"]];
                         animation.type = @"pageUnCurl";
                         animation.fillMode = kCAFillModeForwards;
                         animation.startProgress = 0.35;
                         [animation setRemovedOnCompletion:NO];
                         [self.revealViewController.view.layer addAnimation:animation forKey:@"pageUnCurlAnimation"];
                         [self.infoView removeFromSuperview];
                         ;}
     ];
    
    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:selectedIndexPath animated:NO];
    
    if (self.indexPath != nil) {
            [self.tableView selectRowAtIndexPath:self.indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

@end
