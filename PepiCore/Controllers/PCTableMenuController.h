//
//  TableMenuController.h
//  pepi
//
//  Created by Radek Zmeskal on 13/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCAppDescription.h"
#import "PCActionController.h"

@interface PCTableMenuController : UIViewController <UITableViewDataSource, UITableViewDelegate>

/** app description */
@property (strong, nonatomic) PCAppDescription *appDescription;

@end
