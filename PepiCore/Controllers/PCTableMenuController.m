//
//  TableMenuController.m
//  pepi
//
//  Created by Radek Zmeskal on 13/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCTableMenuController.h"
#import "HexColors.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton+AFNetworking.h"
#import "PCGraphicManager.h"
#import "PCNavigationManager.h"
#import "PCDefaults.h"

#import "PCSection.h"

@interface PCTableMenuController ()

@property ( strong, nonatomic) NSArray *sections;


@property ( strong, nonatomic)  PCActionController *actionMenu;

@property ( strong, nonatomic) UITableView *tableView;

@property UIView *infoView;

@end

@implementation PCTableMenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init info view
    
    self.infoView = [[PCDefaults sharedInstance] getInfoController].view;
    
    self.infoView.translatesAutoresizingMaskIntoConstraints = NO;

    if (self.infoView != nil)
    {
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(curlHideView:)];
        [self.infoView addGestureRecognizer:singleFingerTap];
    }
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];
    
    // init view
    
    self.view.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    
    self.tableView.allowsSelection = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    NSDictionary *bindings = [NSDictionary  dictionaryWithObjectsAndKeys:self.tableView, @"tableView", nil];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|" options:0 metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:bindings]];
    
    // load data
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    
    self.sections = [self.appDescription.sections sortedArrayUsingDescriptors:@[sortDescriptor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.infoView == nil)
    {
        return self.sections.count;
    }
    
    return self.sections.count + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellMenu"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    // init views

    UIImageView *imageBackground = [[UIImageView alloc] init];
    UIButton *button = [[UIButton alloc] init];
    UIImageView *image = [[UIImageView alloc] init];
    UILabel *label = [[UILabel alloc] init];
    
    [image setTintColor:[PCGraphicManager sharedManager].menuTextColor];

    imageBackground.backgroundColor = [PCGraphicManager sharedManager].menuColor;
    label.textColor = [PCGraphicManager sharedManager].menuTextColor;
    
    imageBackground.translatesAutoresizingMaskIntoConstraints = NO;
    button.translatesAutoresizingMaskIntoConstraints = NO;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    image.contentMode = UIViewContentModeScaleAspectFit;
    
    [cell.contentView addSubview:imageBackground];
    [cell.contentView addSubview:image];
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:button];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    // set constrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings( imageBackground, button, label, image);
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[imageBackground]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[imageBackground]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[button]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[image(20.0)]" options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-25-[label]-25-|" options:0 metrics:nil views:bindings]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[image(20)]-20-[label]-30-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:bindings]];
    
    [label sizeToFit];
    
    [[cell contentView] sizeToFit];
    [[cell contentView] updateConstraints];
    
    // load data
    
    if ((indexPath.row >= self.sections.count) && (self.infoView != nil))
    {
        label.text = @"O aplikaci";
        
        image.image = [UIImage imageNamed:@"pepi_info"];
    }
    else {
        PCSection *section = self.sections[indexPath.row];
        
        image.image = [[PCGraphicManager sharedManager] imageForURL:section.icon];
        
        label.text = section.name;
    }        
    
    button.tag = indexPath.row;
    
    [button addTarget:self action:@selector(clickMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    image.image = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction

-(IBAction)clickMenu:(id)sender
{
    if (([sender tag] == self.sections.count)  && (self.infoView != nil))
    {
        [self performSelector:@selector(curlShowView:) withObject:nil];
        
        return;
    }
    
    PCSection *section = self.sections[((UIButton*)sender).tag];
    
    if (section.viewController == nil)
    {
        [section createViewController];
    }
    
    [self.navigationController pushViewController:section.viewController animated:YES];
}

#pragma mark - Curl functionality

- (IBAction)curlShowView:(id)sender
{
    
    [UIView animateWithDuration:1.0
                     animations:^{
                         CATransition *animation = [CATransition animation];
//                         [animation setDelegate:self];
                         [animation setDuration:0.7];
                         [animation setTimingFunction:[CAMediaTimingFunction functionWithName:@"default"]];
                         animation.type = @"pageCurl";
                         animation.fillMode = kCAFillModeForwards;
                         animation.endProgress = 0.6;
                         [animation setRemovedOnCompletion:NO];
                         [self.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
                         
                         [self.view addSubview:self.infoView];
                         
                         NSDictionary *bindings = [NSDictionary dictionaryWithObject:self.infoView forKey:@"curlView"];
                         
                         [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[curlView]|" options:0 metrics:nil views:bindings]];
                         [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[curlView]|" options:0 metrics:nil views:bindings]];
                     }
     ];
}

- (IBAction)curlHideView:(id)sender
{
    [UIView animateWithDuration:1.0
                     animations:^{
                         CATransition *animation = [CATransition animation];
//                         [animation setDelegate:self];
                         [animation setDuration:0.7];
                         [animation setTimingFunction:[CAMediaTimingFunction functionWithName:@"default"]];
                         animation.type = @"pageUnCurl";
                         animation.fillMode = kCAFillModeForwards;
                         animation.startProgress = 0.35;
                         [animation setRemovedOnCompletion:NO];
                         [self.view.layer addAnimation:animation forKey:@"pageUnCurlAnimation"];
                         [self.infoView removeFromSuperview];
                         ;}
     ];
}

@end
