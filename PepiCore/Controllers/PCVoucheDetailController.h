//
//  PCVoucheDetailController.h
//  PepiCore
//
//  Created by Radek Zmeskal on 3/6/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCVoucher.h"

@interface PCVoucheDetailController : UIViewController

@property PCVoucher *voucher;

@end
