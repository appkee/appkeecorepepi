//
//  PCVoucheDetailController.m
//  PepiCore
//
//  Created by Radek Zmeskal on 3/6/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCVoucheDetailController.h"
#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "PCGraphicManager.h"
#import "UIImageView+AFNetworking.h"
#import "PCDataManager.h"
#import "PCDefaults.h"

@interface PCVoucheDetailController ()

@property UIButton *button;

@end

@implementation PCVoucheDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
//    UIBarButtonItem *barButtonUse = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(clickDone:)];
//    
//    self.navigationItem.rightBarButtonItem = barButtonUse;
    
//    self.view.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // init views
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
    
    UIImageView *leftTop = [UIImageView new];
    [leftTop setImage:[UIImage imageNamed:@"left_top" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    leftTop.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:leftTop];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[leftTop(30)]" options:0 metrics:nil views:@{@"leftTop":leftTop}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[leftTop(30)]" options:0 metrics:nil views:@{@"leftTop":leftTop}]];
    
    UIImageView *top = [UIImageView new];
    [top setImage:[UIImage imageNamed:@"top" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    top.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:top];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-40-[top]-40-|" options:0 metrics:nil views:@{@"top":top}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[top(30)]" options:0 metrics:nil views:@{@"top":top}]];
    
    UIImageView *rightTop = [UIImageView new];
    [rightTop setImage:[UIImage imageNamed:@"right_top" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    rightTop.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:rightTop];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:[rightTop(30)]-10-|" options:0 metrics:nil views:@{@"rightTop":rightTop}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[rightTop(30)]" options:0 metrics:nil views:@{@"rightTop":rightTop}]];
    
    UIImageView *right = [UIImageView new];
    [right setImage:[UIImage imageNamed:@"right" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    right.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:right];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:[right(30)]-10-|" options:0 metrics:nil views:@{@"right":right}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[right]-40-|" options:0 metrics:nil views:@{@"right":right}]];

    
    UIImageView *rightBottom = [UIImageView new];
    [rightBottom setImage:[UIImage imageNamed:@"right_bottom" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    rightBottom.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:rightBottom];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:[rightBottom(30)]-10-|" options:0 metrics:nil views:@{@"rightBottom":rightBottom}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[rightBottom(30)]-10-|" options:0 metrics:nil views:@{@"rightBottom":rightBottom}]];

    
    UIImageView *bottom = [UIImageView new];
    [bottom setImage:[UIImage imageNamed:@"bottom" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    bottom.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:bottom];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-40-[bottom]-40-|" options:0 metrics:nil views:@{@"bottom":bottom}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[bottom(30)]-10-|" options:0 metrics:nil views:@{@"bottom":bottom}]];

    UIImageView *bottomLeft = [UIImageView new];
    [bottomLeft setImage:[UIImage imageNamed:@"left_bottom" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    bottomLeft.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:bottomLeft];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[bottomLeft(30)]" options:0 metrics:nil views:@{@"bottomLeft":bottomLeft}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[bottomLeft(30)]-10-|" options:0 metrics:nil views:@{@"bottomLeft":bottomLeft}]];

    UIImageView *left = [UIImageView new];
    [left setImage:[UIImage imageNamed:@"left" inBundle:frameworkBundle compatibleWithTraitCollection:nil]];
    left.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:left];
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[left(30)]" options:0 metrics:nil views:@{@"left":left}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[left]-40-|" options:0 metrics:nil views:@{@"left":left}]];

    
    
    
    
    UIScrollView *scrollView = [UIScrollView new];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:scrollView];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[scrollView]-30-|" options:0 metrics:nil views:@{@"scrollView":scrollView}]];
//    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-30-[scrollView]|" options:0 metrics:nil views:@{@"scrollView":scrollView}]];
    
    UIView *viewBackgound = [UIView new];
//    viewBackgound.backgroundColor = [PCGraphicManager sharedManager].headerColor;
    viewBackgound.translatesAutoresizingMaskIntoConstraints = NO;
    
    [scrollView addSubview:viewBackgound];
    [scrollView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewBackgound(==scrollView)]|" options:0 metrics:nil views:@{@"viewBackgound":viewBackgound, @"scrollView":scrollView}]];
    [scrollView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[viewBackgound]-|" options:0 metrics:nil views:@{@"viewBackgound":viewBackgound, @"scrollView":scrollView}]];
    
    UILabel *labelTitle = [UILabel new];
    labelTitle.numberOfLines = 0;
    labelTitle.textAlignment = NSTextAlignmentCenter;
    labelTitle.font = [UIFont systemFontOfSize:24.0];
//    labelTitle.textColor = [PCGraphicManager sharedManager].headerTextColor;
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    [viewBackgound addSubview:labelTitle];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[labelTitle]|" options:0 metrics:nil views:@{@"labelTitle":labelTitle}]];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelTitle(>=40)]" options:0 metrics:nil views:@{@"labelTitle":labelTitle}]];
    
    UIImageView *image = [UIImageView new];
    image.backgroundColor = [UIColor clearColor];
    image.contentMode = UIViewContentModeScaleAspectFit;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    
    [viewBackgound addSubview:image];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[image]-2-|" options:0 metrics:nil views:@{@"image":image, @"labelTitle":labelTitle}]];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[labelTitle]-[image(120)]" options:0 metrics:nil views:@{@"image":image, @"labelTitle":labelTitle}]];
    
    UILabel *labelName = [UILabel new];
    labelName.numberOfLines = 0;
    labelName.textAlignment = NSTextAlignmentCenter;
//    labelDate.textColor = [PCGraphicManager sharedManager].headerTextColor;
    labelName.textColor = [UIColor whiteColor];
    labelName.backgroundColor = [UIColor blackColor];
    labelName.translatesAutoresizingMaskIntoConstraints = NO;
    
    [viewBackgound addSubview:labelName];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[labelName]-|" options:0 metrics:nil views:@{@"image":image, @"labelName":labelName}]];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[image]-[labelName(>=30)]" options:0 metrics:nil views:@{@"image":image, @"labelName":labelName}]];
    
    UILabel *labelDescription = [UILabel new];
    labelDescription.numberOfLines = 0;
//    labelDescription.textColor = [PCGraphicManager sharedManager].contentTextColor;
//    labelDescription.backgroundColor = [PCGraphicManager sharedManager].contentColor;
    labelDescription.textColor = [UIColor blackColor];
    labelDescription.translatesAutoresizingMaskIntoConstraints = NO;
    
    [viewBackgound addSubview:labelDescription];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[labelDescription]-2-|" options:0 metrics:nil views:@{@"labelDescription":labelDescription, @"labelName":labelName}]];
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[labelName]-16-[labelDescription(>=50)]" options:0 metrics:nil views:@{@"labelDescription":labelDescription, @"labelName":labelName}]];
    
    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[labelDescription]-|" options:0 metrics:nil views:@{ @"labelDescription":labelDescription}]];

    [viewBackgound updateConstraints];
    
    
    self.button = [UIButton new];
//    [button setTitleColor:[PCGraphicManager sharedManager].contentTextColor forState:UIControlStateNormal];
//    button.backgroundColor = [PCGraphicManager sharedManager].contentColor;
    [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.button.backgroundColor = [UIColor redColor];
//    button.tintColor = [PCGraphicManager sharedManager].contentTextColor;
    self.button.translatesAutoresizingMaskIntoConstraints = NO;
    [self.button addTarget:self action:@selector(clickUse:) forControlEvents:UIControlEventTouchUpInside];
    
//    [viewBackgound addSubview:button];
//    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|" options:0 metrics:nil views:@{@"labelDescription":labelDescription, @"button":button}]];
//    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[labelDescription]-[button]" options:0 metrics:nil views:@{@"labelDescription":labelDescription, @"button":button}]];
//
//    [viewBackgound addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button]-|" options:0 metrics:nil views:@{ @"button":button}]];
    
    [self.view addSubview:self.button];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[button]-50-|" options:0 metrics:nil views:@{@"button":self.button}]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-30-[scrollView]-[button(40)]-40-|" options:0 metrics:nil views:@{@"button":self.button, @"scrollView":scrollView}]];
    
    [self.view updateConstraints];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    
    NSDate *date = [formatter dateFromString:self.voucher.validity];
    
    formatter.dateFormat = @"hh:mm dd.MM.yyyy";
    
    labelTitle.text = @"APP KUPÓN";
    labelName.text = self.voucher.name;
//    labelDate.text = [NSString stringWithFormat:@"Platnost: do %@", [formatter stringFromDate:date]];
//        labelDate.text = [NSString stringWithFormat:@"Platnost: do %@", self.voucher.validity];
    labelDescription.text = self.voucher.detail;
    [self.button setTitle:@"UPLATNIT KUPÓN" forState:UIControlStateNormal];
    
//    NSURL *url = [NSURL URLWithString:[[PCGraphicManager sharedManager] createUrl:self.voucher.image]];
//    [image setImageWithURL:url placeholderImage:nil];
    
    NSString *urlString = nil;
    if ([self.voucher.image hasPrefix:@".."])
    {
        urlString = [self.voucher.image stringByReplacingOccurrencesOfString:@".." withString:@"http://pepiapp.cz.uvirt43.active24.cz"];
    }
    else {
        urlString = [@"http://pepiapp.cz.uvirt43.active24.cz/app-manager/img/ikony/" stringByAppendingString:self.voucher.image];
    }
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [image setImageWithURL:url];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *vouchers = [defaults objectForKey:@"vouchers"];
    
    NSNumber *used = [vouchers objectForKey:[NSString stringWithFormat:@"%ld", [self.voucher.identificator integerValue]]];
    if ((used != nil) && ([used boolValue]))
    {
        [self.button setEnabled:false];
        
        self.button.backgroundColor = [UIColor grayColor];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)clickUse:(id)sender
{
//    [[PCDataManager sharedManager] useVouchersWithCode:[[PCDefaults sharedInstance] getAppCode] voucher:self.voucher.identificator viewController:self success:^(NSArray *array) {
//        
//    } failure:^{
//
//    }];
//
//    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Použít kupón"
                                          message:@"Zadejde odemykací kód"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction
                                  actionWithTitle:@"UPLATNIT"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      UITextField *login = alertController.textFields.firstObject;
                                      
                                      [[PCDataManager sharedManager] useVouchersWithCode:[[PCDefaults sharedInstance] getAppCode] voucher:self.voucher.identificator confirm:login.text viewController:self success:^(BOOL result) {
                                          
                                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Kupón uplatněn" message:nil delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil, nil];
                                          
                                          [alert show];
                                          
                                          [self.button setEnabled:false];
                                          
                                          self.button.backgroundColor = [UIColor grayColor];
                                          
//                                          NSLog(@"tt");
                                      } failure:^{
//                                          NSLog(@"tt");
                                      }];
                                  }];
    
    okAction.enabled = false;
    
    UIAlertAction *cancelAction = [UIAlertAction
                                  actionWithTitle:@"Zrušit"
                                  style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction *action)
                                  {
//                                      NSLog(@"Reset action");
//                                      [alertController dismissViewControllerAnimated:YES completion:nil];
                                  }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Kod";
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
         
     }];
    
    UITextField *login = alertController.textFields.firstObject;
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    login.becomeFirstResponder;
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *login = alertController.textFields.firstObject;
        UIAlertAction *okAction = alertController.actions.lastObject;
        okAction.enabled = login.text.length > 2;
    }
}


-(IBAction)clickDone:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
