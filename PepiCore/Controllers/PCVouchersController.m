//
//  VoucherController.m
//  PepiCore
//
//  Created by Radek Zmeskal on 3/5/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCVouchersController.h"

#import "PCGraphicManager.h"
#import "PCNavigationManager.h"
#import "PCTitleManager.h"
#import "NSString+HTML.h"
#import "PCVoucher.h"
#import "PCDataManager.h"
#import "PCDefaults.h"
#import "PCVoucheDetailController.h"
#import "UIImageView+AFNetworking.h"
#import "PCActionController.h"


@interface PCVouchersController ()

@property ( strong, nonatomic) PCActionController *actionMenu;

@property NSArray *vouchers;

@end

@implementation PCVouchersController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init nav bar
    
    [[PCNavigationManager sharedManager] showTitleWithViewController:self];
    
    self.actionMenu = [[PCActionController alloc] initWithViewController:self type:MenuActionTypeControllers];
        
//    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(clickDone:)];
//    
//    self.navigationItem.rightBarButtonItem = barButtonDone;
    
    // init views
    
    self.tableView.backgroundColor = [[PCGraphicManager sharedManager] contentColor];
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    self.tableView.estimatedSectionHeaderHeight = [[PCTitleManager sharedManager] titleHeight];
    
    self.tableView.separatorColor = [[PCGraphicManager sharedManager] contentTextColor];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[PCDataManager sharedManager] vouchersWithCode:[[PCDefaults sharedInstance] getAppCode] viewController:self success:^(NSArray *array)
     {
//        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
         
//        self.vouchers = [array sortedArrayUsingDescriptors:@[sortDescriptor]];
         
         self.vouchers = array;
         
         if (self.vouchers.count == 0)
         {
             UILabel *label = [[UILabel alloc] init];
             label.text = @"Nejsou aktivní žadné kupóny";
             label.textAlignment = NSTextAlignmentCenter;
             label.font = [UIFont boldSystemFontOfSize:18.0];
             label.textColor = [PCGraphicManager sharedManager].contentTextColor;
             
             [self.tableView setBackgroundView:label];
         } else
         {
             [self.tableView setBackgroundView:nil];
         }
         
         [self.tableView reloadData];
     } failure:^{
         
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.vouchers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellArticle0"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    PCVoucher *voucher = self.vouchers[indexPath.row];
    
    // init views
    
    UILabel *labelTitle = [[UILabel alloc] init];
    labelTitle.numberOfLines = 2;
    labelTitle.font = [UIFont boldSystemFontOfSize:18.0];
    labelTitle.adjustsFontSizeToFitWidth=YES;
    labelTitle.minimumScaleFactor = 0.5;
    labelTitle.textColor = [PCGraphicManager sharedManager].headerTextColor;
    
    UIImageView *image = [[UIImageView alloc] init];
    image.contentMode = UIViewContentModeScaleAspectFill;
    image.clipsToBounds = YES;
    
    UIImageView *imageBackground = [[UIImageView alloc] init];
    imageBackground.contentMode = UIViewContentModeScaleAspectFill;
    imageBackground.clipsToBounds = YES;
    imageBackground.backgroundColor = [PCGraphicManager sharedManager].headerColor;
    
    labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    imageBackground.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:imageBackground];
    [cell.contentView addSubview:image];
    [cell.contentView addSubview:labelTitle];
    
    
    // set contrainsts
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(image, imageBackground, labelTitle);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[imageBackground]-|" options:0 metrics:nil views:bindings]];
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[image(80)]-16-|" options:0 metrics:nil views:bindings]];
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[labelTitle]-16-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[imageBackground]-8-|" options:0 metrics:nil views:bindings]];
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-16-[image(70)]-[labelTitle]-16-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    // load data
 
    NSString *urlString = nil;
    if ([voucher.image hasPrefix:@".."])
    {
        urlString = [voucher.image stringByReplacingOccurrencesOfString:@".." withString:@"http://pepiapp.cz.uvirt43.active24.cz"];
    }
    else {
        urlString = [@"http://pepiapp.cz.uvirt43.active24.cz/app-manager/img/ikony/" stringByAppendingString:voucher.image];
    }
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [image setImageWithURL:url];
    
    labelTitle.text = voucher.name;
    
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [[PCTitleManager sharedManager] createTitleRowWithTitle:@"APP KUPÓN"];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PCVoucher *voucher = self.vouchers[indexPath.row];
    
    PCVoucheDetailController *voucherController = [[PCVoucheDetailController alloc] init];
    voucherController.voucher = voucher;
    
    [self.navigationController pushViewController:voucherController animated:YES];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)clickDone:(id)sender
{
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

@end
