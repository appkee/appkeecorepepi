//
//  MenuActionController.h
//  pepi
//
//  Created by Radek Zmeskal on 26/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/**
 * Type of menu
 */
typedef enum {
    MenuActionTypeControllers,
    MenuActionTypeControllersLink
} MenuActionType;

/**
 *  Menu action class
 */
@interface PCActionController : NSObject <UIActionSheetDelegate, UIAlertViewDelegate>

/**
 *  init menu action on wiew contrller
 *
 *  @param viewController actual view controller
 *  @param type           type of menu
 *
 *  @return instance
 */
-(instancetype)initWithViewController:(UIViewController *)viewController type:(MenuActionType)type;

@end
