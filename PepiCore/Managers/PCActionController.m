//
//  MenuActionController.m
//  pepi
//
//  Created by Radek Zmeskal on 26/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCActionController.h"
#import "PCGraphicManager.h"
#import "PCLinkController.h"
#import "PCRSSController.h"
#import "PCDefaults.h"
#import "PCVouchersController.h"
#import "PCSelfieController.h"
#import "PCDefaults.h"
#import "PCGraphicManager.h"

@interface PCActionController ()

@property ( strong, nonatomic) UIViewController *controller;

@property ( strong, nonatomic) UIActionSheet *actionSheet;

@property ( atomic) MenuActionType typ;

@end



@implementation PCActionController

-(instancetype)initWithViewController:(UIViewController *)viewController type:(MenuActionType)type
{
    self = [super init];
    if (self)
    {
        self.controller = viewController;
        
        NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
        UIImage *imageDots = [UIImage imageNamed:@"dots" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
        
        UIBarButtonItem *barButtonMenu = [[UIBarButtonItem alloc] initWithImage:imageDots style:UIBarButtonItemStylePlain target:self action:@selector(clickMenu:)];
        
        [barButtonMenu setTintColor:[PCGraphicManager sharedManager].headerTextColor];
        
        self.typ = type;
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        [array addObject:@"Aktualizovat aplikaci"];
        
        if ([[PCDefaults sharedInstance] getPhotobox])
        {
            [array addObject:@"Fotobox"];
        }        
        
        if (type == MenuActionTypeControllers)
        {
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:nil];
            
            if ([[PCDefaults sharedInstance] getTester])
            {
                [self.actionSheet setDestructiveButtonIndex:[self.actionSheet addButtonWithTitle:@"Seznam aplikací"]];
                
                for (NSString *title in array) {
                    [self.actionSheet addButtonWithTitle: title];
                }
                
                [self.actionSheet setCancelButtonIndex: [self.actionSheet addButtonWithTitle:@"Zrušit"]];
                
            }
            else {
                for (NSString *title in array) {
                    [self.actionSheet addButtonWithTitle: title];
                }
                
                [self.actionSheet setCancelButtonIndex: [self.actionSheet addButtonWithTitle:@"Zrušit"]];
            }
        }
        
        if (type == MenuActionTypeControllersLink)
        {
            NSArray *array2 = [@[@"Aktualizovat"] arrayByAddingObjectsFromArray:array];
            
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:nil];
            
            if ([[PCDefaults sharedInstance] getTester])
            {
                [self.actionSheet setDestructiveButtonIndex:[self.actionSheet addButtonWithTitle:@"Seznam aplikací"]];
                
                for (NSString *title in array) {
                    [self.actionSheet addButtonWithTitle: title];
                }
                
                [self.actionSheet setCancelButtonIndex: [self.actionSheet addButtonWithTitle:@"Zrušit"]];
                
            }
            else {
                for (NSString *title in array) {
                    [self.actionSheet addButtonWithTitle: title];
                }
                
                [self.actionSheet setCancelButtonIndex: [self.actionSheet addButtonWithTitle:@"Zrušit"]];
            }
        }
        
        viewController.navigationItem.rightBarButtonItem = barButtonMenu;
                
        self.actionSheet.delegate = self;
    }
    
    return self;
}

#pragma mark - IBActions

-(IBAction)clickMenu:(id)sender
{
    [self.actionSheet showInView:self.controller.view];
}

#pragma mark -action sheets delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (![[PCDefaults sharedInstance] getTester])
    {
        buttonIndex++;
    }
    
    if (buttonIndex == 0)
    {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"nPCClose" object:nil];
        
        [self.controller dismissViewControllerAnimated:YES completion:nil];
        
        return;
        
    }
    
    if (buttonIndex == 1)
    {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"nPCReload" object:nil];
        
        [self.controller dismissViewControllerAnimated:YES completion:nil];
        
        return;
    }
    
    if (self.typ != MenuActionTypeControllersLink)
    {
        buttonIndex++;
    }
    
    if (buttonIndex == 2)
    {
        if ([self.controller isKindOfClass:[PCLinkController class]])
        {
            [(PCLinkController*)self.controller loadLink];
        }
        
        if ([self.controller isKindOfClass:[PCRSSController class]])
        {
            [(PCRSSController*)self.controller loadLink];
        }
        
        return;
    }
    
    if (![[PCDefaults sharedInstance] getVoucher])
    {
        buttonIndex++;
    }
    
    if (![[PCDefaults sharedInstance] getPhotobox])
    {
        buttonIndex++;
    }
    
    if (buttonIndex == 3)
    {
        PCSelfieController *controller = [[PCSelfieController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        
        navigationController.navigationBar.translucent = NO;
        navigationController.navigationBar.barTintColor = [PCGraphicManager sharedManager].headerColor;
        navigationController.navigationBar.tintColor = [PCGraphicManager sharedManager].headerTextColor;
        [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [PCGraphicManager sharedManager].headerTextColor}];
        
        controller.name = @"Fotobox";
        
        UIImage *img = [[PCGraphicManager sharedManager] imageForURL:[[PCDefaults sharedInstance] getSelfie]];
        
        controller.logo = img;
        
        [self.controller presentViewController:navigationController animated:YES completion:nil];
    }
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"PepiStoryboard" bundle:nil];
        PCVouchersController *vc = [sb instantiateViewControllerWithIdentifier:@"voucherController"];
        
        [self.controller presentViewController:vc animated:YES completion:nil];
    }
    
    if (buttonIndex == 1)
    {
        
    }
}

@end
