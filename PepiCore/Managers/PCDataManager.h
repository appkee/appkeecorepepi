//
//  DataManager.h
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PCSection.h"
#import "PCPepiCore.h"

@class PCAppDescription;

/**
 * Data manager class
 */
@interface PCDataManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (id)sharedManager;


/**
 *  load appp description
 *
 *  @param viewController actual controller
 *  @param code           identificator to load
 *  @param success        success block
 *  @param errorHandler   error handler block
 */
-(void)appDescriptionWithCode:(NSNumber*) code delegate:(id<PCPepiCoreDelegate>) delegate success:(void (^)(PCAppDescription* app)) success failure:(void (^)())errorHandler;

/**
 *  load appp description for apppliaction
 *
 *  @param code           identificator to load
 *  @param success        success block
 *  @param errorHandler   error handler block
 */
-(void)appDescriptionAppWithCode:(NSNumber *)code delegate:(id<PCPepiCoreDelegate>) delegate success:(void (^)(PCAppDescription *))success failure:(void (^)())errorHandler
;

/**
 *  send form
 *
 *  @param viewController actual controller
 *  @param identificator  identificator to send
 *  @param message        message to send
 *  @param success        success block
 */
-(void)sendFormDataWithID:(UIViewController*) viewController identificator:(NSNumber*) identificator message:(NSString*) message success:(void (^)()) success;


/**
 *  load appp description for apppliaction
 *
 *  @param code           identificator to load
 *  @param success        success block
 *  @param errorHandler   error handler block
 */
-(void)vouchersWithCode:(NSNumber *)code viewController:(UIViewController*) viewController success:(void (^)(NSArray *))success failure:(void (^)())errorHandler;



/**
 *  Use voucher for apppliaction
 *
 *  @param code           identificator to load
 *  @param voucher           identificator to load
 *  @param success        success block
 *  @param errorHandler   error handler block
 */
-(void)useVouchersWithCode:(NSNumber *)code voucher:(NSNumber*) voucher confirm:(NSString*)confirm viewController:(UIViewController*) viewController success:(void (^)(BOOL))success failure:(void (^)())errorHandler;

@end
