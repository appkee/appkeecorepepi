     //
//  DataManager.m
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCDataManager.h"

#import <MBProgressHUD.h>

#import "PCNetworkManager.h"
#import "PCAppDescription.h"
#import "PCGraphicManager.h"
#import "PCFileManager.h"
#import "PCDefaults.h"
#import "PCGraphicManager.h"
#import "PCVoucher.h"

#define NEW_USER @"new_user"

@interface PCDataManager ()

//@property ( atomic) BOOL loaded;


@end

@implementation PCDataManager

+ (id)sharedManager
{
    static PCDataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)appDescriptionWithCode:(NSNumber *)code delegate:(id<PCPepiCoreDelegate>) delegate success:(void (^)(PCAppDescription *))success failure:(void (^)())errorHandler
{
    [delegate PCPepiCore:nil progressInfo:@"Stahuji aktualní data" progress:0.1];
    
    // request
    [[PCNetworkManager sharedManager] getAppDescriptionWithCode:code new:false success:^(NSDictionary *result)
     {
//         [hud hide:YES];
         
         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
             
             if (result == nil)
             {
                 [delegate PCPepiCore:nil errorTitle:@"Chyba připojení" errorMessage:@"Zkontrolujte si prosím připojení k internetu." retry:false];
                 
//                 errorHandler();
                 
                 return;
             }
             
             NSError* error = nil;
             PCAppDescription *appDescription = [[PCAppDescription alloc] initWithDictionary:result error:&error];
             
             if (error)
             {
                 [delegate PCPepiCore:nil errorTitle:[error localizedFailureReason] errorMessage:[error localizedDescription] retry:false];
                 
//                 errorHandler();
                 
                 return;
             }
             
             //         NSLog(@"%@", appDescription);
             
             NSString *loaded = [[PCGraphicManager sharedManager] createWithAppDescription:appDescription delegate:delegate];
             
             if (loaded)
             {
                 NSString *message = [@"Nebyly nahrány tyto obrázky:\n\n" stringByAppendingString:loaded];
                 message = [message stringByAppendingString:@"\nZkontrolujte připojení k internetu a aktualizujte aplikaci. Pokud je připojení vpořádku. Zkontrolujte zda byly tyto obrázky správně nahrány v PEPI administrátoru."];
                 [delegate PCPepiCore:nil errorTitle:@"Upozornění" errorMessage:message retry:false];
             }             
             
            [delegate PCPepiCore:nil progressInfo:@"Generuji grafické rozhraní" progress:1.0];
             
             
             success(appDescription);
             
         });
         
     } failure:^(NSError *error) {
         
         // show error message
         
         [delegate PCPepiCore:nil errorTitle:[error localizedFailureReason] errorMessage:[error localizedDescription] retry:false];
         
         errorHandler();
     }];
        
//    });
}

-(void)appDescriptionAppWithCode:(NSNumber *)code delegate:(id<PCPepiCoreDelegate>) delegate success:(void (^)(PCAppDescription *))success failure:(void (^)())errorHandler
{
    [delegate PCPepiCore:nil progressInfo:@"Stahuji aktualní data" progress:0.1];
    
    BOOL new = false;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults.dictionaryRepresentation.allKeys containsObject:NEW_USER])
    {
        new = true;
    }
    
    [defaults setBool:NO forKey:NEW_USER];
    // request
    [[PCNetworkManager sharedManager] getAppDescriptionWithCode:code new:new success:^(NSDictionary *result)
     {
         //         [hud hide:YES];
         
         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
         
             if (result == nil)
             {
                 [delegate PCPepiCore:nil errorTitle:@"Chyba připojení" errorMessage:@"Zkontrolujte si prosím připojení k internetu." retry:true];
                 
                 return;
             }
             
             [[PCFileManager sharedManager] writeDictionary:result];
             
             NSError* error = nil;
             PCAppDescription *appDescription = [[PCAppDescription alloc] initWithDictionary:result error:&error];
             
             if (error)
             {
                 [delegate PCPepiCore:nil errorTitle:[error localizedFailureReason] errorMessage:[error localizedDescription] retry:true];
                 
                 return;
             }
             
             NSString *loaded = [[PCGraphicManager sharedManager] createWithAppDescription:appDescription delegate:delegate];
             
             if (loaded)
             {
                 [delegate PCPepiCore:nil errorTitle:@"Upozornění" errorMessage:@"Nebyly nahrány všechny obrázky. Zkontrolujte připojení k internetu a aktualizujte aplikaci." retry:false];
             }
             
            [delegate PCPepiCore:nil progressInfo:@"Generuji grafické rozhraní" progress:1.0];
             
//             self.loaded = true;
             
             success(appDescription);

         });
         
         
     } failure:^(NSError *error) {
         
         NSDictionary *result = [[PCFileManager sharedManager] loadDictionary];
         
         if (result != nil)
         {
             PCAppDescription *appDescription = [[PCAppDescription alloc] initWithDictionary:result error:nil];
             
             [[PCGraphicManager sharedManager] createWithAppDescription:appDescription delegate:delegate];
             
             success(appDescription);
             
             [delegate PCPepiCore:nil errorTitle:@"Upozornění" errorMessage:@"Aplikace nebyla aktualizována." retry:false];
                                       
             return;
         }
         
         errorHandler();
     }];
}

-(void)sendFormDataWithID:(UIViewController*) viewController identificator:(NSNumber*) identificator message:(NSString*) message success:(void (^)()) success
{
    // show progress
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    
    hud.backgroundColor = [PCGraphicManager sharedManager].menuColor;
    hud.color = [PCGraphicManager sharedManager].menuTextColor;
    
    // request
    [[PCNetworkManager sharedManager] sendFormWithSectionID:identificator message:message success:^{
        
        [MBProgressHUD hideHUDForView:viewController.view animated:true];
         
         success();
         
     } failure:^(NSError *error) {
         
        [MBProgressHUD hideHUDForView:viewController.view animated:true];
         
         // show error message
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedFailureReason] message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         
         [alert show];
     }];
}

-(void)vouchersWithCode:(NSNumber *)code viewController:(UIViewController*) viewController success:(void (^)(NSArray *))success failure:(void (^)())errorHandler
{
    // show progress
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    
    hud.backgroundColor = [PCGraphicManager sharedManager].menuColor;
    hud.color = [PCGraphicManager sharedManager].menuTextColor;
    
    // request
    [[PCNetworkManager sharedManager] getVouchersWithCode:code success:^(NSDictionary *result)
     {
         [MBProgressHUD hideHUDForView:viewController.view animated:true];
         
         if (result == nil)
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Chyba načtení" message:@"Zkontrolujte si prosím připojení k internetu" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [alert show];
             
             errorHandler();
             
             return;
         }
         
         NSError* error = nil;

         NSArray *vouchers = (NSArray*)[result objectForKey:@"vouchers"];
         
         NSMutableArray *array = [[NSMutableArray alloc] init];
         
         for (NSDictionary *voucher in vouchers)
         {
             PCVoucher *newVoucher = [[PCVoucher alloc] initWithDictionary:voucher error:&error];
             
             if (error)
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedFailureReason] message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 
                 [alert show];
                 
                 errorHandler();
                 
                 return;
             }
             
             [array addObject:newVoucher];
         }
         
         success(array);
         
     } failure:^(NSError *error) {
         
         [MBProgressHUD hideHUDForView:viewController.view animated:true];
         
         // show error message
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedFailureReason] message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         
         [alert show];
         
         errorHandler();
     }];
}

-(void)useVouchersWithCode:(NSNumber *)code voucher:(NSNumber*) voucher confirm:(NSString*) confirm viewController:(UIViewController*) viewController success:(void (^)(BOOL))success failure:(void (^)())errorHandler
{
    // show progress
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    
    hud.backgroundColor = [PCGraphicManager sharedManager].menuColor;
    hud.color = [PCGraphicManager sharedManager].menuTextColor;
    
    // request
    [[PCNetworkManager sharedManager] useVoucherWithCode:code voucher:voucher confirm:confirm success:^(NSDictionary *result)
     {
         [MBProgressHUD hideHUDForView:viewController.view animated:true];
         
         if (result == nil)
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Chyba načtení" message:@"Zkontrolujte si prosím připojení k internetu" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [alert show];
             
             errorHandler();
             
             return;
         }
         
//         NSLog(@"%@", result);
         
         NSNumber *uplatnen = [result objectForKey:@"uplatnen"];
         
         if ([uplatnen boolValue])
         {
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             NSMutableDictionary *vouchers = [defaults objectForKey:@"vouchers"];
             
             if (vouchers == nil)
             {
                 vouchers = [[NSMutableDictionary alloc] init];
             }
             else {
                 vouchers = [vouchers mutableCopy];
             }
             
             [vouchers setValue:[NSNumber numberWithBool:true] forKey:[NSString stringWithFormat:@"%ld", (long)[voucher integerValue]]];
             
             [defaults setObject:vouchers forKey:@"vouchers"];
             
             [defaults synchronize];
             
             success(true);
          }
         else {
             NSString * message = [result objectForKey:@"errorMessage"];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [alert show];
             
             errorHandler();
         }
         
         
         
     } failure:^(NSError *error) {
         
         [MBProgressHUD hideHUDForView:viewController.view animated:true];
         
         // show error message
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedFailureReason] message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         
         [alert show];
         
         errorHandler();
     }];
}


@end
