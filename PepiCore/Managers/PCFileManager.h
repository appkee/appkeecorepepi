//
//  FileManager.h
//  pepi
//
//  Created by Radek Zmeskal on 12/22/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCFileManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (PCFileManager*)sharedManager;


-(void)writeDictionary:(NSDictionary*) dataDictionary;

-(NSDictionary*)loadDictionary;


@end
