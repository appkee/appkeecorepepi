//
//  GraphicManager.h
//  pepi
//
//  Created by Radek Zmeskal on 19/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "PCAppDescription.h"
#import "PCPepiCore.h"

/**
 *  Graphic manager class
 */
@interface PCGraphicManager : NSObject

/**
 *  header color
 */
@property (strong, nonatomic) UIColor* headerColor;
/**
 *  header test color
 */
@property (strong, nonatomic) UIColor* headerTextColor;
/**
 *  menu color
 */
@property (strong, nonatomic) UIColor* menuColor;
/**
 *  menu text color
 */
@property (strong, nonatomic) UIColor* menuTextColor;
/**
 *  content color
 */
@property (strong, nonatomic) UIColor* contentColor;
/**
 *  content text color
 */
@property (strong, nonatomic) UIColor* contentTextColor;

/**
 *  content text color
 */
@property (strong, nonatomic) NSString* contentTextColorHEX;

/**
 *  logo color
 */
@property (strong, nonatomic) UIColor* logo;
/**
 *  show logo
 */
@property (strong, nonatomic) NSNumber* onlyLogo;


/**
 *  shared instance
 *
 *  @return class
 */
+ (PCGraphicManager*)sharedManager;

/**
 *  init class wit app description
 *
 *  @param appDescription app description
 *
 *  @return images loaded
 */
-(NSString*)createWithAppDescription:(PCAppDescription*) appDescription delegate:(id<PCPepiCoreDelegate>) delegate;

/**
 *  get uiimage to url
 *
 *  @param url url string
 *
 *  @return image
 */
-(UIImage*)imageForURL:(NSString*) url;

-(NSString *)createUrl:(NSString*) link;

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;

@end
