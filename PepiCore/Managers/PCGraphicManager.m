    //
//  GraphicManager.m
//  pepi
//
//  Created by Radek Zmeskal on 19/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCGraphicManager.h"
#import "HexColors.h"
#import "PCSection.h"
#import "PCImage.h"

@interface PCGraphicManager ()

@property ( strong, nonatomic) NSMutableDictionary *images;

@property ( strong, nonatomic) NSString *defaultDirectory;


@end

@implementation PCGraphicManager

+ (PCGraphicManager*)sharedManager
{
    static PCGraphicManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.defaultDirectory = [NSTemporaryDirectory() stringByAppendingPathComponent:@"images"];
    }
    return self;
}

-(NSString*)createWithAppDescription:(PCAppDescription*) appDescription delegate:(id<PCPepiCoreDelegate>) delegate
{
    self.headerColor = [UIColor hx_colorWithHexString:appDescription.headerColor];
    self.headerTextColor = [UIColor hx_colorWithHexString:appDescription.headerTextColor];
    self.menuColor = [UIColor hx_colorWithHexString:appDescription.menuColor];
    self.menuTextColor = [UIColor hx_colorWithHexString:appDescription.menuTextColor];
    self.contentColor = [UIColor hx_colorWithHexString:appDescription.contentColor];
    self.contentTextColor = [UIColor hx_colorWithHexString:appDescription.contentTextColor];
    self.contentTextColorHEX = appDescription.contentTextColor;

    // load urls
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    // add voucher img
    [array addObject:@"0160.png"];
    
        if ((appDescription.logo != nil) && (appDescription.logo.length != 0))
    {
        [array addObject:appDescription.logo];
//    [array addObject:appDescription.loadingImage];
    }
    
    for (PCSection *section in appDescription.sections)
    {
        if (section.icon != nil)
        {
            if (section.icon.length != 0)
            {
                [array addObject:section.icon];
            }
        }
        
        for (PCImage *image in section.images)
        {
            if ((image.url == nil) || (image.url.length == 0))
            {
                continue;
            }
            
            [array addObject:image.url];
        }
    }
    
    [array addObject:@"../app-manager/img/varning.png"];
    
    self.images = [[NSMutableDictionary alloc] init];
    
    BOOL directory = YES;
    
    // load home folder of app
    NSString *appDirectory = [self.defaultDirectory stringByAppendingPathComponent:appDescription.name];
    BOOL find = [[NSFileManager defaultManager] fileExistsAtPath:appDirectory isDirectory:&directory];

    if (!find)
    {
        NSError *error;

        [[NSFileManager defaultManager] createDirectoryAtPath:appDirectory withIntermediateDirectories:YES attributes:nil error:&error];
        
        NSLog(@"%@ %@", error.localizedFailureReason, error.localizedDescription);
    }
    
    if (delegate != nil)
    {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [delegate PCPepiCore:nil progressInfo:@"Aktualizace obrázků" progress:0.1];
        });
        
    }
    
    // load images
    for (int i=0; i<array.count; i++)
    {
        NSString *link = array[i];
        // get url
        
        NSString *url = nil;
        if ([link hasPrefix:@".."])
        {
            url = [link stringByReplacingOccurrencesOfString:@".." withString:@"http://pepiapp.cz.uvirt43.active24.cz"];
        }
        else {
            url = [@"http://pepiapp.cz.uvirt43.active24.cz/app-manager/img/ikony/" stringByAppendingString:link];
        }
        
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        // test if downloaded
        BOOL directory = NO;
        
        NSString *linkFile = [[self.defaultDirectory stringByAppendingPathComponent:appDescription.name] stringByAppendingPathComponent:[url lastPathComponent]];
        
        BOOL find = [[NSFileManager defaultManager] fileExistsAtPath:linkFile isDirectory:&directory];
        
        if (!find)
        {
            // save image
            NSURL *urlImage= [NSURL URLWithString:url];
            NSData * dataImage = [NSData dataWithContentsOfURL:urlImage];
            
            if (dataImage == nil)
            {
                NSLog(@"error");
            }

            [dataImage writeToFile:linkFile atomically:YES];
            
            if (delegate != nil)
            {
                float progress = ((float)i/(float)array.count)*100;
                
                NSString *info = [NSString stringWithFormat:@"Stahování nových obrázků %.0f%%", progress];
                
                [delegate PCPepiCore:nil progressInfo:info progress:(progress/90) + 0.1];
            }
        }
        
        [self.images setObject:linkFile forKey:link];
    }
    
    NSString *errors = @"";
    int count = 0;
    
    for (NSString *url in array)
    {
        NSString *linkFile = [self.images objectForKey:url];
        
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:linkFile];
        
        if (image == nil)
        {
            if (count > 5)
            {
                errors = [errors stringByAppendingString: @"a další\n"];
                return errors;
            }
            
            count++;
            
            NSRange range = [url rangeOfString:@"/" options:NSBackwardsSearch];
            if (range.location != NSNotFound)
            {
                errors = [errors stringByAppendingString: [url substringFromIndex:range.location + 1]];
                errors = [errors stringByAppendingString: @"\n"];
            }
        }
    }
    
    if (errors.length == 0)
    {
        return nil;
    }
    
    return errors;
}

-(UIImage *)imageForURL:(NSString *)url
{    
    NSString *linkFile = [self.images objectForKey:url];
    
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:linkFile];
    
//    if (image == nil)
//    {
//        image = [[UIImage alloc] init];
//    }
    
    return image;
}

-(NSString *)createUrl:(NSString*) link
{
    NSString *url = nil;
    if ([link hasPrefix:@".."])
    {
        url = [link stringByReplacingOccurrencesOfString:@".." withString:@"http://pepiapp.cz.uvirt43.active24.cz"];
    }
    else {
        url = [@"http://pepiapp.cz.uvirt43.active24.cz/app-manager/img/ikony/" stringByAppendingString:link];
    }
    
    return url;
}

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

@end
