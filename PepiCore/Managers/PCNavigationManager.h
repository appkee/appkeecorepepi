//
//  NavigationManager.h
//  pepi
//
//  Created by Radek Zmeskal on 20/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Navigation manager class
 */
@interface PCNavigationManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (PCNavigationManager*)sharedManager;

/**
 *  init title with image
 *
 *  @param image title image
 */
-(void)createTitleViewWithImage:(UIImage*) image;

/**
 *  init title with image and text
 *
 *  @param image     title image
 *  @param name      title text
 *  @param textColor title text color
 */
-(void)createTitleViewWithImage:(UIImage*) image name:(NSString *)name textColor:(UIColor*) textColor;

/**
 *  show title in controller
 *
 *  @param viewController view controller
 */
-(void)showTitleWithViewController:(UIViewController*) viewController;



@end
