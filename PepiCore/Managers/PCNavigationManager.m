//
//  NavigationManager.m
//  pepi
//
//  Created by Radek Zmeskal on 20/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCNavigationManager.h"

@interface PCNavigationManager ()

@property ( strong, nonatomic) UIView *topView;

@property ( strong, nonatomic) UIImage *img;
@property ( strong, nonatomic) NSString *title;
@property ( strong, nonatomic) UIColor *color;

@end

@implementation PCNavigationManager

+ (PCNavigationManager*)sharedManager
{
    static PCNavigationManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)createTitleViewWithImage:(UIImage*) image
{
    self.img = image;
    self.title = nil;
    self.color = nil;
    
    return;
}

-(void)createTitleViewWithImage:(UIImage*) image name:(NSString *)name textColor:(UIColor*) textColor
{
    self.img = image;
    self.title = name;
    self.color = textColor;
    
    return;
}

-(void)showTitleWithViewController:(UIViewController*) viewController
{
    // calculate hight
    float targetHeight = viewController.navigationController.navigationBar.frame.size.height - 4;
    
    float targetWidth = viewController.navigationController.navigationBar.frame.size.width - 80;
    
    float scale = 0;
    
    if (self.img != nil)
    {
        scale = self.img.size.width/self.img.size.height;
    }
    
    float width = scale * targetHeight;
    
    // init image
    UIImageView *logoView = [[UIImageView alloc] initWithImage:self.img];
    [logoView setFrame:CGRectMake(0, 2, width, targetHeight)];
    [logoView setContentMode:UIViewContentModeScaleAspectFit];
    
    // only image
    if (self.title == nil)
    {
        viewController.navigationItem.titleView = logoView;
        
        return;
    }
    
    // init views
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, targetWidth, targetHeight)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(width, 0, 50, targetHeight)];
    label.text = self.title;
    label.textColor = self.color;
    label.adjustsFontSizeToFitWidth=YES;
    label.minimumScaleFactor=0.5;
    label.textAlignment = NSTextAlignmentCenter;
    
//    view.translatesAutoresizingMaskIntoConstraints = NO;
    logoView.translatesAutoresizingMaskIntoConstraints = NO;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    [view addSubview:logoView];
    [view addSubview:label];
    
    // set constraints
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings( logoView, label);
    
    NSString *str = [NSString stringWithFormat:@"H:|[logoView(%.1f)]-[label]|", width];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:str options:0 metrics:nil views:bindings]];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[logoView]-2@900-|" options:0 metrics:nil views:bindings]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:bindings]];
    
    [view sizeToFit];
    
    
    // assign title view
    viewController.navigationItem.titleView = view;
}

@end
