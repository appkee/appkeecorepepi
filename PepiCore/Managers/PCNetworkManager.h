//
//  NetworkManager.h
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 * Network handler class
 */
@interface PCNetworkManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (id)sharedManager;

/**
 *  get app description
 *
 *  @param code         identification of app
 *  @param new          new instalation
 *  @param success      success block
 *  @param errorHandler error handler block
 */
-(void)getAppDescriptionWithCode:(NSNumber *) code new:(BOOL) new success:(void (^)(NSDictionary *result))success failure:(void (^)(NSError *))errorHandler;

/**
 *  send filled form
 *
 *  @param identificator identificator section
 *  @param message       message to send
 *  @param success       success block
 *  @param errorHandler  error handler block
 */
-(void)sendFormWithSectionID:(NSNumber *) identificator message:(NSString*) message success:(void (^)())success failure:(void (^)(NSError *))errorHandler;

/**
 *  get voucher
 *
 *  @param code         identification of app
 *  @param success      success block
 *  @param errorHandler error handler block
 */
-(void)getVouchersWithCode:(NSNumber *) code success:(void (^)(NSDictionary *result))success failure:(void (^)(NSError *))errorHandler;

-(void)useVoucherWithCode:(NSNumber *) code voucher:(NSNumber*) voucher confirm:(NSString*) confirm success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))errorHandler;


@end
