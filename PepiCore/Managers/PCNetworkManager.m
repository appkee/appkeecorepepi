//
//  NetworkManager.m
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCNetworkManager.h"
#import "NSString+MD5.h"
#import "AFNetworking.h"


//#define URL_APPS @"http://pepiapp.cz/app-manager/device-data-loader/get-tester-apps.php"
#define PC_URL_APP_DESCRIPTION @"http://pepiapp.cz/app-manager/device-data-loader/get-app-data-v2.php"
//#define URL_APP_DESCRIPTION @"http://pepiapp.cz/app-manager/device-data-loader/get-app-data.php"
#define PC_URL_SEND_FORM @"http://pepiapp.cz/app-manager/device-data-loader/send-app-form.php"
#define PC_URL_GET_VOUCHERS @"http://pepiapp.cz/app-manager/device-data-loader/get-vouchers.php"
#define PC_URL_USE_VOUCHER @"http://pepiapp.cz/app-manager/device-data-loader/uplatni-voucher.php"


@implementation PCNetworkManager

+ (id)sharedManager
{
    static PCNetworkManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


-(void)getAppDescriptionWithCode:(NSNumber *)code new:(BOOL) new success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    //    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:code, @"app-code", nil];
    
    if (new != false) {
        [parameters setValue:[NSString stringWithFormat:@"%d", new] forKey:@"iam-new"];
    }
    
    [manager GET:PC_URL_APP_DESCRIPTION parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
//         NSLog(@"%@", responseObject);
         
         NSData *data = responseObject;
         
         if ((responseObject == nil) || [responseObject isKindOfClass:[NSNull class]])
         {
             NSError *error = [NSError errorWithDomain:@"Chyba" code:1 userInfo:nil];
             errorHandler(error);
             
             return;
         }
         
         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         NSLog(@"%@", newStr);         
         
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
//         NSLog(@"%@", json);
         
         success(json);
         
//         dispatch_async(operation.completionQueue ?: dispatch_get_main_queue(), ^(void) {
//             success(json);
//         });
         
     } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         errorHandler(error);
     }];
    
    //    NSLog(@"%@", manager);
    
}

-(void)sendFormWithSectionID:(NSNumber *) identificator message:(NSString*) message success:(void (^)())success failure:(void (^)(NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    //    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:identificator, @"sekce", message, @"zprava", nil];
    
    [manager POST:PC_URL_SEND_FORM parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
         NSLog(@"%@", responseObject);
         
         NSData *data = responseObject;
         
         if ((responseObject == nil) || [responseObject isKindOfClass:[NSNull class]])
         {
             NSError *error = [NSError errorWithDomain:@"Chyba" code:1 userInfo:nil];
             errorHandler(error);
             
             return;
         }
         
         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         NSLog(@"%@", newStr);                  
         
         success();
         
     } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         errorHandler(error);
     }];
}

-(void)getVouchersWithCode:(NSNumber *)code success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:code, @"app-code", nil];
    
    [manager POST:PC_URL_GET_VOUCHERS parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
         //         NSLog(@"%@", responseObject);
         
         NSData *data = responseObject;
         
         if ((responseObject == nil) || [responseObject isKindOfClass:[NSNull class]])
         {
             NSError *error = [NSError errorWithDomain:@"Chyba" code:1 userInfo:nil];
             errorHandler(error);
             
             return;
         }
         
         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         //         NSLog(@"%@", newStr);
         
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         //         NSLog(@"%@", json);
         
         success(json);
         
     } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         errorHandler(error);
     }];
}

-(void)useVoucherWithCode:(NSNumber *) code voucher:(NSNumber*) voucher confirm:(NSString*) confirm success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:code, @"app-code", voucher, @"voucher-code", confirm, @"confirm-code", nil];
    
    [manager POST:PC_URL_USE_VOUCHER parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
         //         NSLog(@"%@", responseObject);
         
         NSData *data = responseObject;
         
         if ((responseObject == nil) || [responseObject isKindOfClass:[NSNull class]])
         {
             NSError *error = [NSError errorWithDomain:@"Chyba" code:1 userInfo:nil];
             errorHandler(error);
             
             return;
         }
         
         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         //         NSLog(@"%@", newStr);
         
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         //         NSLog(@"%@", json);
         
         success(json);
         
     } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         errorHandler(error);
     }];
}

@end
