//
//  PCNotification.h
//  PepiCore
//
//  Created by Radek Zmeskal on 4/10/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCNotification : NSObject

+(void)showNotificationWithTitle:(NSString*) title detail:(NSString*) detail;

@end
