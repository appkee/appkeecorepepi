//
//  PCNotification.m
//  PepiCore
//
//  Created by Radek Zmeskal on 4/10/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCNotification.h"
#import "PCNotificationController.h"

@implementation PCNotification

+(void)showNotificationWithTitle:(NSString*) title detail:(NSString*) detail
{
//    UIViewController *viewController = [UIApplication sharedApplication].keyWindow.visibleViewController;
    
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    PCNotificationController *controller = [[PCNotificationController alloc] init];
    controller.title = title;
    controller.detail = detail;
    
    [topController presentViewController:topController animated:YES completion:nil];
}

@end
