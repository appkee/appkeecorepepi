//
//  TitleManager.h
//  pepi
//
//  Created by Radek Zmeskal on 23/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/**
 * Title manager class
 */
@interface PCTitleManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (PCTitleManager*)sharedManager;

/**
 *  create title row
 *
 *  @param title title text
 *
 *  @return title table view cell
 */
-(UITableViewCell*)createTitleRowWithTitle:(NSString*) title;

/**
 *  get default height of title
 *
 *  @return height
 */
-(float)titleHeight;

@end
