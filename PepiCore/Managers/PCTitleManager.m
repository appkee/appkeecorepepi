//
//  TitleManager.m
//  pepi
//
//  Created by Radek Zmeskal on 23/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCTitleManager.h"
#import "PCGraphicManager.h"

@implementation PCTitleManager

+ (PCTitleManager*)sharedManager
{
    static PCTitleManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(UITableViewCell*)createTitleRowWithTitle:(NSString *)title
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellViewTitle"];
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
//    label.lineBreakMode = NSLineBreakByClipping;
    
    UIImageView *image = [[UIImageView alloc] init];
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    
    [cell.contentView addSubview:image];
    [cell.contentView addSubview:label];
    
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(label, image);
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[label]-8-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[image]|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image]|" options:0 metrics:nil views:bindings]];
    
    [[cell contentView] updateConstraints];
    
    label.text = [title uppercaseString];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont boldSystemFontOfSize:20.0];
    
    label.textColor = [PCGraphicManager sharedManager].headerTextColor;
    
    cell.backgroundColor = [PCGraphicManager sharedManager].contentColor;
    cell.contentView.backgroundColor = [PCGraphicManager sharedManager].contentColor;
    cell.backgroundView.backgroundColor = [PCGraphicManager sharedManager].contentColor;
    
    image.backgroundColor = [PCGraphicManager sharedManager].headerColor;
    
    
    return cell;
}


-(float)titleHeight
{
    return 50;
}

@end
