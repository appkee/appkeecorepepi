//
//  AppDescription.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
//#import "PCActionController.h"

@class PCSection;
@protocol PCSection;


@interface PCAppDescription : JSONModel<SWRevealViewControllerDelegate>

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSNumber* menuType;
@property (strong, nonatomic) NSString* headerColor;
@property (strong, nonatomic) NSString* headerTextColor;
@property (strong, nonatomic) NSString* menuColor;
@property (strong, nonatomic) NSString* menuTextColor;
@property (strong, nonatomic) NSString* contentColor;
@property (strong, nonatomic) NSString* contentTextColor;
@property (strong, nonatomic) NSString* logo;
@property (strong, nonatomic) NSNumber* onlyLogo;
@property (strong, nonatomic) NSString* loadingImage;
@property (strong, nonatomic) NSNumber* lockUp;
@property (strong, nonatomic) NSArray<PCSection>* sections;
@property (strong, nonatomic) NSString<Optional>* css;

@property (strong, nonatomic) NSNumber* fotoboxUse;
@property (strong, nonatomic) NSNumber* vouchersUse;

/**
 *  create view controller
 *
 *  @return view controller
 */
-(UIViewController*)createController;

@end
