//
//  AppDescription.m
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCAppDescription.h"
#import "PCSideMenuController.h"
#import "PCTableMenuController.h"
#import "PCBarMenuController.h"
#import "HexColors.h"
#import "SWRevealViewController.h"
#import "PCGraphicManager.h"
#import "PCNavigationManager.h"
//#import "PCActionController.h"
#import "PCSection.h"
#import "PCEmptyController.h"
#import "PCDefaults.h"
#import "PCVouchersController.h"

@interface PCAppDescription ()

//@property ( strong, nonatomic) PCActionController<Optional> *actionMenu;

@property ( strong, nonatomic) UIViewController<Optional> *controller;

@end

@implementation PCAppDescription


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"nazev": @"name",
                                                       @"typMenu": @"menuType",
                                                       @"barvaHlavicky": @"headerColor",
                                                       @"barvaHlavickyText": @"headerTextColor",
                                                       @"barvaMenu": @"menuColor",
                                                       @"barvaMenuText": @"menuTextColor",
                                                       @"barvaObsah": @"contentColor",
                                                       @"barvaObsahText": @"contentTextColor",
                                                       @"logo": @"logo",
                                                       @"jenLogo": @"onlyLogo",
                                                       @"loadingImage": @"loadingImage",
                                                       @"uzamknout": @"lockUp",
                                                       @"sekce": @"sections",
                                                       @"fotoboxUse": @"fotoboxUse",
                                                       @"vouchersUse": @"vouchersUse",
                                                       @"css": @"css",
                                                       
                                                       }];
}


-(UIViewController*)createController
{
    [[PCDefaults sharedInstance] photobox:self.fotoboxUse];
    [[PCDefaults sharedInstance] selfie:self.logo];
    [[PCDefaults sharedInstance] voucher:self.vouchersUse];
    [[PCDefaults sharedInstance] css:self.css];
    
    // create title
    UIImage *image= [[PCGraphicManager sharedManager] imageForURL:self.logo];
    if ([self.onlyLogo boolValue])
    {
        [[PCNavigationManager sharedManager] createTitleViewWithImage:image];
    }
    else {
        [[PCNavigationManager sharedManager] createTitleViewWithImage:image name:self.name textColor:[[PCGraphicManager sharedManager] headerTextColor]];
    }

    if ((self.sections == nil) || (self.sections.count == 0))
    {
        PCSection *section = [[PCSection alloc] init];
        section.name = @"Ukázka";
        section.icon = @"../app-manager/img/varning.png";
        section.type = @"warning";
        
        self.sections = [[NSArray<PCSection>  alloc] initWithObjects:section, nil];
    }
    
    if ([[PCDefaults sharedInstance] getVoucher])
    {
        PCSection *sectionCoupon = [[PCSection alloc] init];
        sectionCoupon.name = @"APP KUPÓN";
        sectionCoupon.icon = @"0160.png";
        sectionCoupon.type = @"Voucher";
        sectionCoupon.position = [NSNumber numberWithInteger:INT_MAX];
        
        NSMutableArray<PCSection> *temp = [self.sections mutableCopy];
        [temp addObject:sectionCoupon];
        
        self.sections = temp;
    }
    
    switch (self.menuType.intValue)
    {
        case 0:
            self.controller = [self sideMenu];
            break;
        case 1:
            self.controller = [self barMenu];
            break;
        case 2:
            self.controller = [self tableMenu];
            break;
            
        default:
            break;
    }
    
    return self.controller;
}

/**
 *  create side controller
 *
 *  @return view controller
 */
-(UIViewController*)sideMenu
{
    UINavigationController *frontNavigationController = nil;
    PCSideMenuController *rearViewController = [[PCSideMenuController alloc] init];
 
    // test empty
    if ((self.sections == nil) || (self.sections.count == 0))
    {
        PCEmptyController *frontController = [[PCEmptyController alloc] init];
        
        frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontController];
        frontNavigationController.navigationBar.translucent = NO;
        frontNavigationController.navigationBar.barTintColor = [UIColor hx_colorWithHexString:self.headerColor];
        frontNavigationController.navigationBar.tintColor = [UIColor hx_colorWithHexString:self.headerTextColor];
        [frontNavigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor hx_colorWithHexString:self.headerTextColor]}];
    }
    else {
        // sort section
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        
        NSArray *sections = [self.sections sortedArrayUsingDescriptors:@[sortDescriptor]];

        rearViewController.appDescription = self;
        rearViewController.sections = sections;
        
        // load first section
        PCSection *section = sections[0];
        
        if (!section.navigationController)
        {
            [section createNavigationController];
        }
        
        frontNavigationController = section.navigationController;
    }
    
    // create rear menu
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontNavigationController];
    
    mainRevealController.bounceBackOnOverdraw = NO;
    mainRevealController.stableDragOnOverdraw = YES;
    mainRevealController.rearViewRevealWidth = 200;
    mainRevealController.rearViewRevealOverdraw = 0;
    
    [mainRevealController panGestureRecognizer];
    mainRevealController.delegate = self;
    
//    rearViewController.revealViewController = mainRevealController;
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[self class]];
    UIImage *imageDots = [UIImage imageNamed:@"reveal-icon" inBundle:frameworkBundle compatibleWithTraitCollection:nil];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:imageDots style:UIBarButtonItemStylePlain target:mainRevealController action:@selector(revealToggle:)];
    
//    [mainRevealController panGestureRecognizer];
    
    frontNavigationController.topViewController.navigationItem.leftBarButtonItem = revealButtonItem;
    
    return mainRevealController;
}

/**
 *  create tab bar controller
 *
 *  @return view controller
 */
-(UIViewController*)barMenu
{
    PCBarMenuController *barMenuController = [[PCBarMenuController alloc] init];
    
    barMenuController.appDescription = self;
    
    return barMenuController;    
}

/**
 *  create table view controller
 *
 *  @return view controller
 */
-(UIViewController*)tableMenu
{
    // create controller
    PCTableMenuController *tableMenu = [[PCTableMenuController alloc] init];
    tableMenu.appDescription = self;
    
    // set navigation bar
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:tableMenu];
    
    navigationController.navigationBar.translucent = NO;
    navigationController.navigationBar.barTintColor = [UIColor hx_colorWithHexString:self.headerColor];
    navigationController.navigationBar.tintColor = [UIColor hx_colorWithHexString:self.headerTextColor];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor hx_colorWithHexString:self.headerTextColor]}];
    
    
    return navigationController;
}

#pragma mark - SWRevealController

- (NSString*)stringFromFrontViewPosition:(FrontViewPosition)position
{
    NSString *str = nil;
    if ( position == FrontViewPositionLeft ) str = @"FrontViewPositionLeft";
    if ( position == FrontViewPositionRight ) str = @"FrontViewPositionRight";
    if ( position == FrontViewPositionRightMost ) str = @"FrontViewPositionRightMost";
    if ( position == FrontViewPositionRightMostRemoved ) str = @"FrontViewPositionRightMostRemoved";
    return str;
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController willRevealRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didRevealRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController willHideRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didHideRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController willShowFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didShowFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController willHideFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didHideFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}


@end
