//
//  Article.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCContent.h"

@class PCText;
@class PCImage;


@interface PCArticle : PCContent

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* type;
@property (strong, nonatomic) NSString* date;
@property (strong, nonatomic) NSNumber* show;


@property NSArray<Optional> *texts;
@property NSArray<Optional> *images;
@property NSArray<Optional> *videos;

@end



@protocol PCArticle

@end
