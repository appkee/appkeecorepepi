//
//  Article.m
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCArticle.h"

@implementation PCArticle

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"identificator",
                                                       @"id_sekce": @"idSection",
                                                       @"jmeno": @"name",
                                                       @"typ": @"type",
                                                       @"poradi": @"position",
                                                       @"datum": @"date",
                                                       @"zobrazit": @"show",
                                                       
                                                       }];
}


@end
