//
//  Content.h
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PCContent : JSONModel

@property (strong, nonatomic) NSNumber* identificator;
@property (strong, nonatomic) NSNumber<Optional>* idSection;
@property (strong, nonatomic) NSNumber* position;

@end
