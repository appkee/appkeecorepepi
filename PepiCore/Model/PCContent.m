//
//  Content.m
//  pepi
//
//  Created by Radek Zmeskal on 15/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCContent.h"

@implementation PCContent

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"identificator",
                                                       @"id_sekce": @"idSection",
                                                       @"poradi": @"position"
                                                       
                                                       }];
}

@end
