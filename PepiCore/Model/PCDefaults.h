//
//  PCDefaults.h
//  PepiCore
//
//  Created by Radek Zmeskal on 2/14/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PCDefaults : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (id)sharedInstance;


-(void)setAppCode: (NSNumber *) appCode infoController:(UIViewController*) controller tester:(BOOL) tester;

-(UIViewController*)getInfoController;

-(BOOL)getTester;

-(NSNumber*)getAppCode;


-(void)photobox:(NSNumber*)photobox;

-(BOOL)getPhotobox;

-(void)voucher:(NSNumber*)voucher;

-(BOOL)getVoucher;


-(void)selfie:(NSString*)selfie;

-(NSString*)getSelfie;

-(void)css:(NSString*)css;

-(NSString*)getCss;

@end
