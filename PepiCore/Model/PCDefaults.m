//
//  PCDefaults.m
//  PepiCore
//
//  Created by Radek Zmeskal on 2/14/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCDefaults.h"


@interface PCDefaults ()

@property ( strong, nonatomic) UIViewController *infoController;

@property BOOL tester;

@property NSNumber *appCode;

@property (nonatomic)  BOOL photobox;

@property (nonatomic) BOOL voucher;

@property (nonatomic) NSString *selfie;

@property (nonatomic) NSString *css;

@end

@implementation PCDefaults

+ (id)sharedInstance
{
    static PCDefaults *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)setAppCode: (NSNumber *) appCode infoController:(UIViewController*) controller tester:(BOOL) tester
{
    self.appCode = appCode;
    self.infoController = controller;
    self.tester = tester;
}

-(UIViewController*)getInfoController
{
    return self.infoController;
}

-(BOOL)getTester
{
    return self.tester;
}

-(NSNumber*)getAppCode
{
    return self.appCode;
}

-(void)photobox:(NSNumber*)photobox
{
    if (photobox == nil)
    {
        self.photobox = false;
        return;
    }
    
    self.photobox = photobox.boolValue;
}

-(BOOL)getPhotobox
{
    return self.photobox;
}

-(void)voucher:(NSNumber*)voucher
{
    if (voucher == nil)
    {
        self.voucher = false;
        return;
    }
    
    self.voucher = voucher.boolValue;
}

-(BOOL)getVoucher
{
    return self.voucher;
}

-(void)selfie:(NSString*)selfie
{
    self.selfie = selfie;
}

-(NSString*)getSelfie
{
    return self.selfie;
}

-(void)css:(NSString*)css
{
    self.css = css;
}

-(NSString*)getCss
{
    return self.css;
}

@end
