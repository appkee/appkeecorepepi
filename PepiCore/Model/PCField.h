//
//  Field.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class PCFieldsItem;

@interface PCField : JSONModel

@property (strong, nonatomic) NSNumber* identificator;
@property (strong, nonatomic) NSNumber<Optional>* idSection;
@property (strong, nonatomic) NSString<Optional>* name;
@property (strong, nonatomic) NSString* type;
@property (strong, nonatomic) NSNumber* position;

@property NSArray<Optional> *items;

@property NSString<Optional> *value;

@end


@protocol PCField

@end
