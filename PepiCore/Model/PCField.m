//
//  Field.m
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCField.h"

@implementation PCField

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"identificator",
                                                       @"id_sekce": @"idSection",
                                                       @"nazev": @"name",
                                                       @"typ": @"type",
                                                       @"poradi": @"position",
                                                       
                                                       }];
}


@end
