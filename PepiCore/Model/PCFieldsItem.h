//
//  FieldsItem.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PCFieldsItem : JSONModel

@property (strong, nonatomic) NSNumber* identificator;
@property (strong, nonatomic) NSNumber* idField;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSNumber* position;

@property NSNumber<Optional> *selected;

@end


@protocol PCFieldsItem

@end