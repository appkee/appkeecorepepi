//
//  FieldsItem.m
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCFieldsItem.h"

@implementation PCFieldsItem

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"identificator",
                                                       @"id_field": @"idField",
                                                       @"nazev": @"name",
                                                       @"poradi": @"position",
                                                       
                                                       }];
}

@end
