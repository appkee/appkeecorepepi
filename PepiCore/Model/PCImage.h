//
//  Image.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCContent.h"

@interface PCImage : PCContent

@property (strong, nonatomic) NSString* url;
@property (strong, nonatomic) NSNumber<Optional>* idArticle;

@end

@protocol PCImage

@end