//
//  RSSItem.h
//  pepi
//
//  Created by Radek Zmeskal on 23/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  RSS feed item class
 */
@interface PCRSSItem : NSObject

/**
 *  title of message
 */
@property NSString *title;
/**
 *  date of message
 */
@property NSString *date;
/**
 *  detail description
 */
@property NSString *summary;
/**
 *  link to article
 */
@property NSString *link;

/**
 *  height of item;
 */
@property NSNumber *height;

/**
 *  init rss feed item
 *
 *  @param title  title
 *  @param date   date
 *  @param sumary content
 *  @param link   link
 *
 *  @return instance
 */
-(instancetype)initWithTitle:(NSString*) title date:(NSString*) date sumary:(NSString*) sumary link:(NSString*)link;

@end
