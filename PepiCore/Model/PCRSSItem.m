//
//  RSSItem.m
//  pepi
//
//  Created by Radek Zmeskal on 23/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCRSSItem.h"

@implementation PCRSSItem

-(instancetype)initWithTitle:(NSString*) title date:(NSString*) date sumary:(NSString*) sumary link:(NSString*)link
{
    self = [super init];
    if (self)
    {
        self.title = title;
        self.date = date;
        self.summary = sumary;
        self.link = link;
    }
    return self;
}

@end
