//
//  Section.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSONModel/JSONModel.h>
#import "PCImage.h"
#import "PCText.h"
#import "PCArticle.h"
#import "PCField.h"
#import "PCFieldsItem.h"
#import "PCVideo.h"

@interface PCSection : JSONModel

@property (strong, nonatomic) NSNumber* identificator;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* type;
@property (strong, nonatomic) NSString* icon;
@property (strong, nonatomic) NSNumber* position;
@property (strong, nonatomic) NSNumber* show;
@property (strong, nonatomic) NSNumber<Optional>* hideImages;
@property (strong, nonatomic) NSArray<Optional, PCText>* texts;
@property (strong, nonatomic) NSArray<Optional, PCImage>* images;
@property (strong, nonatomic) NSString<Optional>* rssLink;
@property (strong, nonatomic) NSArray<Optional, PCArticle>* articles;
@property (strong, nonatomic) NSString<Optional>* link;
@property (strong, nonatomic) NSString<Optional>* email;
@property (strong, nonatomic) NSString<Optional>* confirm;
@property (strong, nonatomic) NSArray<Optional, PCField>* fields;
@property (strong, nonatomic) NSArray<Optional, PCFieldsItem>* fieldsItem;
@property (strong, nonatomic) NSArray<Optional, PCVideo>* videos;
@property (strong, nonatomic) NSNumber<Optional>* latitude;
@property (strong, nonatomic) NSNumber<Optional>* longitude;

/**
 *  view controller
 */
@property (strong, nonatomic) UIViewController<Optional> *viewController;
/**
 *  navigation controller
 */
@property (strong, nonatomic) UINavigationController<Optional> *navigationController;

/**
 *  create view controller
 */
-(void)createViewController;
/**
 *  create navigtion bar
 */
-(void)createNavigationController;

@end

@protocol PCSection

@end
