//
//  Section.m
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCSection.h"
#import "PCLinkController.h"
#import "PCRSSController.h"
#import "PCPageController.h"
#import "PCGalleryController.h"
#import "PCArticlesController.h"
#import "PCFormController.h"
#import "PCGraphicManager.h"
#import "PCGPSController.h"
#import "PCEmptyController.h"
#import "PCVouchersController.h"

@implementation PCSection

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"identificator",
                                                       @"jmeno": @"name",
                                                       @"typ": @"type",
                                                       @"ikona": @"icon",
                                                       @"poradi": @"position",
                                                       @"zobrazit": @"show",
                                                       @"skryt_obrazky": @"hideImages",
                                                       @"texty": @"texts",
                                                       @"obrazky": @"images",
                                                       @"rssOdkaz": @"rssLink",
                                                       @"clanky": @"articles",
                                                       @"odkaz": @"link",
                                                       @"email": @"email",
                                                       @"confirm": @"confirm",
                                                       @"fields": @"fields",
                                                       @"fields_item": @"fieldsItem",
                                                       @"videa": @"videos",
                                                       @"latitude": @"latitude",
                                                       @"longitude": @"longitude",
                                                       
                                                       }];
}



-(void)createViewController
{
    UIViewController *controller;
    
    // test type of controller
    if ([self.type isEqualToString:@"Odkaz"])
    {
        PCLinkController *linkController = [[PCLinkController alloc] init];
        linkController.link = self.link;
        linkController.name = self.name;
        
        controller = linkController;
    }
    if ([self.type isEqualToString:@"RSS"])
    {
        PCRSSController *rssController = [[PCRSSController alloc] init];
        rssController.rssFeed = self.rssLink;
        rssController.name = self.name;
        
        controller = rssController;
    }
    if ([self.type isEqualToString:@"Stránka"])
    {
        PCPageController *pageController = [[PCPageController alloc] init];
        pageController.images = self.images;
        pageController.texts = self.texts;        
        pageController.name = self.name;
        pageController.videos = self.videos;
        pageController.identificator = self.identificator;
        
        controller = pageController;
    }
    if ([self.type isEqualToString:@"Galerie"])
    {
        PCGalleryController *galeryController = [[PCGalleryController alloc] init];
        galeryController.images = self.images;
        galeryController.name = self.name;
        
        controller = galeryController;
    
    }
    if ([self.type isEqualToString:@"Články"])
    {
        PCArticlesController *articleController = [[PCArticlesController alloc] init];
        articleController.images = self.images;
        articleController.texts = self.texts;
        articleController.articles = self.articles;
        articleController.videos = self.videos;
        articleController.name = self.name;
        articleController.hideImage = self.hideImages;
        
        controller = articleController;
    }
    if ([self.type isEqualToString:@"Formulář"])
    {
        PCFormController *formController = [[PCFormController alloc] init];
        formController.fileds = self.fields;
        formController.fieldsItems = self.fieldsItem;
        formController.name = self.name;
        formController.confirm = self.confirm;
        formController.email = self.email;
        formController.section = self.identificator;
        
        controller = formController;
    }
    if ([self.type isEqualToString:@"GPS"])
    {
        PCGPSController *gpsController = [[PCGPSController alloc] init];
        gpsController.name = @"Otevřít v aplikaci";
        gpsController.latitude = self.latitude;
        gpsController.longitude = self.longitude;
        
        controller = gpsController;
        
    }
    
    if ([self.type isEqualToString:@"Voucher"])
    {
        PCVouchersController *vouchersController = [[PCVouchersController alloc] init];
        
        controller = vouchersController;
        
    }
    
    if ([self.type isEqualToString:@"warning"])
    {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"PepiStoryboard" bundle:nil];
        PCEmptyController *vc = [[PCEmptyController alloc] init];
        
        controller = vc;
    }
    
    self.viewController = controller;
    
}

-(void)createNavigationController
{
    [self createViewController];
    
    // setup navigation controller
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [PCGraphicManager sharedManager].headerColor;
    self.navigationController.navigationBar.tintColor = [PCGraphicManager sharedManager].headerTextColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [PCGraphicManager sharedManager].headerTextColor}];
}


@end
