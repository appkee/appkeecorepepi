//
//  Text.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCContent.h"

@interface PCText : PCContent

@property (strong, nonatomic) NSString* content;
@property (strong, nonatomic) NSNumber<Optional>* idArticle;

@property (strong, nonatomic) NSNumber<Optional>* height;

@end


@protocol PCText

@end