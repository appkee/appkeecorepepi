//
//  Text.m
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "PCText.h"

@implementation PCText

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"identificator",
                                                       @"id_sekce": @"idSection",
                                                       @"obsah": @"content",
                                                       @"poradi": @"position",
                                                       @"id_clanku": @"idArticle",
                                                       
                                                       }];
}


@end
