//
//  Video.h
//  pepi
//
//  Created by Radek Zmeskal on 1/13/16.
//  Copyright © 2016 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PCContent.h"

@interface PCVideo : PCContent

@property (strong, nonatomic) NSString* url;
@property (strong, nonatomic) NSNumber<Optional>* idArticle;

@end

@protocol PCVideo

@end