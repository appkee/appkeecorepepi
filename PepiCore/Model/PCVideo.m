//
//  Video.m
//  pepi
//
//  Created by Radek Zmeskal on 1/13/16.
//  Copyright © 2016 Tripon. All rights reserved.
//

#import "PCVideo.h"

@implementation PCVideo

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                        @"id": @"identificator",
                                                        @"id_sekce": @"idSection",
                                                        @"poradi": @"position",
                                                        @"url_videa": @"url",
                                                        @"id_clanku": @"idArticle",
                                                       }];
}


@end
