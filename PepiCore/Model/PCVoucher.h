//
//  PCVoucher.h
//  PepiCore
//
//  Created by Radek Zmeskal on 3/6/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PCVoucher : JSONModel

@property NSNumber *identificator;
@property NSString *name;
@property NSString *detail;
@property NSString *validity;
@property NSString *image;
//@property NSNumber *left;


@end

@protocol PCVoucher

@end