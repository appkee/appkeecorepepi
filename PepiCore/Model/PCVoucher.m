//
//  PCVoucher.m
//  PepiCore
//
//  Created by Radek Zmeskal on 3/6/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCVoucher.h"

@implementation PCVoucher

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"identificator",
                                                       @"nazev": @"name",
                                                       @"popis": @"detail",
                                                       @"platnost": @"validity",
                                                       @"img": @"image",
//                                                       @"zbyva": @"left",
                                                       
                                                       
                                                       }];
}


@end
