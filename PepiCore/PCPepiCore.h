//
//  PCPepiCore.h
//  PepiCore
//
//  Created by Radek Zmeskal on 2/14/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PCPepiCore;

@protocol PCPepiCoreDelegate <NSObject>

-(void)PCPepiCore:(PCPepiCore*) pepiCore controllerCreated:(UIViewController*) controller;

-(void)PCPepiCore:(PCPepiCore*) pepiCore reload:(NSObject*) result;

-(void)PCPepiCore:(PCPepiCore*) pepiCore close:(NSObject*) result;

-(void)PCPepiCore:(PCPepiCore *)pepiCore errorTitle:(NSString *)errorTitle errorMessage:(NSString*)errorMessage retry:(BOOL)retry;

-(void)PCPepiCore:(PCPepiCore*) pepiCore progressInfo:(NSString*) info progress:(float) progress;

@end


@interface PCPepiCore : NSObject <UIAlertViewDelegate>

@property (nonatomic, strong) id <PCPepiCoreDelegate> delegate;

-(instancetype)initWithAppCode:(NSNumber*) appCode tester:(BOOL) tester infoController:(UIViewController*) controller;

-(void)createController;

@end