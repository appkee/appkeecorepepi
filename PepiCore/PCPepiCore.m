//
//  PCPepiCore.m
//  PepiCore
//
//  Created by Radek Zmeskal on 2/14/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import "PCPepiCore.h"
#import "PCDataManager.h"
#import "PCAppDescription.h"
#import "PCNotificationSettingsController.h"
#import "PCDefaults.h"

@interface PCPepiCore ()

@property NSNumber *appCode;

@end

@implementation PCPepiCore

-(instancetype)initWithAppCode:(NSNumber*) appCode tester:(BOOL)tester infoController:(UIViewController *)controller
{
    self = [super init];
    if (self)
    {
        self.appCode = appCode;
        
        [[PCDefaults sharedInstance] setAppCode:appCode infoController:controller tester:tester];
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserverForName:@"nPCClose"
                            object:nil
                             queue:nil
                        usingBlock:^(NSNotification *notification)
         {
             [self.delegate PCPepiCore:self close:nil];
         }];
        
        [center addObserverForName:@"nPCReload"
                            object:nil
                             queue:nil
                        usingBlock:^(NSNotification *notification)
         {
             [self.delegate PCPepiCore:self reload:nil];
         }];
                
    }
    return self;
}

-(void)createController
{
    if ([[PCDefaults sharedInstance] getTester])
    {
        [[PCDataManager sharedManager] appDescriptionWithCode:self.appCode delegate:self.delegate success:^(PCAppDescription *appDescription)
         {
             UIViewController *controller = [appDescription createController];
             
             [self.delegate PCPepiCore:self controllerCreated:controller];
             
             [self showNotificationSettings:controller];
             
         } failure:^{
             [self.delegate PCPepiCore:self close:nil];
         }];
    }
    else {
        // load description
        [[PCDataManager sharedManager] appDescriptionAppWithCode:self.appCode delegate:self.delegate success:^(PCAppDescription *appDescription)
         {
             UIViewController *controller = [appDescription createController];
             
             [self.delegate PCPepiCore:self controllerCreated:controller];
             
             [self showNotificationSettings:controller];
             
         } failure:^{
             
             [self.delegate PCPepiCore:self errorTitle:@"Chyba připojení" errorMessage:@"Pro první spuštěni musíte být připojeni k internetu" retry:true];                          
         }];
    }
}

#pragma mark - Alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self createController];
}

-(void)showNotificationSettings:(UIViewController*) controller
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications] || ([[[UIApplication sharedApplication] currentUserNotificationSettings] types] == UIUserNotificationTypeNone)) {
            
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"PepiStoryboard" bundle:[NSBundle bundleForClass:[PCNotificationSettingsController class]]];
                PCNotificationSettingsController *vc = (PCNotificationSettingsController*)[sb instantiateViewControllerWithIdentifier:@"NotificationSettingsController"];
            
                [controller presentViewController:vc animated:NO completion:nil];
            }
        });
}

@end