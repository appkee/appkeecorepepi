//
//  PepiCore.h
//  PepiCore
//
//  Created by Radek Zmeskal on 2/14/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PepiCore.
FOUNDATION_EXPORT double PepiCoreVersionNumber;

//! Project version string for PepiCore.
FOUNDATION_EXPORT const unsigned char PepiCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PepiCore/PublicHeader.h>


#import "PCPepiCore.h"
#import "PCNotification.h"

//#import <AFNetworking.h>
//#import "MBProgressHUD.h"
//#import <JSONModel.h>
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
