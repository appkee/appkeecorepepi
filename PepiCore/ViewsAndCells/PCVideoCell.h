//
//  VideoCell.h
//  pepi
//
//  Created by Radek Zmeskal on 1/14/16.
//  Copyright © 2016 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "YTPlayerView.h"

@interface PCVideoCell : UITableViewCell<AVPlayerViewControllerDelegate, YTPlayerViewDelegate>

@property IBOutlet YTPlayerView *playerView;
//@property YTPlayerView *playerView;

@property UILabel *label;

@end
