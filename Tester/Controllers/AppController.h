//
//  AppController.h
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PepiCore.h"

@interface AppController : UIViewController<UIAlertViewDelegate, PCPepiCoreDelegate>

/** application code */
@property (strong, nonatomic) NSNumber *appCode;

@property (weak, nonatomic) IBOutlet UILabel *labelInfo;
@property (weak, nonatomic) IBOutlet UIProgressView *progressInfo;

@end
