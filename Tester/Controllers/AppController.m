//
//  AppController.m
//  pepi
//
//  Created by Radek Zmeskal on 12/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "AppController.h"
#import "DataManager.h"


@interface AppController ()

@property ( strong, nonatomic) AppDescription *appDescription;

@property PCPepiCore *pepiCore;

@end

@implementation AppController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // register navigation center
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"PepiTesterStoryboard" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"infoController"];
    
    self.pepiCore = [[PCPepiCore alloc] initWithAppCode:self.appCode tester:YES infoController:vc];
//    self.pepiCore = [[PCPepiCore alloc] initWithAppCode:self.appCode tester:YES infoController:nil];
    self.pepiCore.delegate = self;

    [self.pepiCore createController];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserverForName:@"nPCProgress"
                        object:nil
                         queue:nil
                    usingBlock:^(NSNotification *notification)
     {
         self.labelInfo.text = notification.object;
         
         [self.labelInfo setNeedsDisplay];
     }];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)PCPepiCore:(PCPepiCore *)pepiCore controllerCreated:(UIViewController *)controller
{    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self presentViewController:controller animated:NO completion:nil];
    });        
    
//    sleep(2);
}

-(void)PCPepiCore:(PCPepiCore *)pepiCore reload:(NSObject *)result
{
    [self.pepiCore createController];
}

-(void)PCPepiCore:(PCPepiCore *)pepiCore close:(NSObject *)result
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
    
}

-(void)PCPepiCore:(PCPepiCore *)pepiCore errorTitle:(NSString *)errorTitle errorMessage:(NSString *)errorMessage retry:(BOOL)retry
{
    
//    [self dismissViewControllerAnimated:YES completion:^{
//        [self dismissViewControllerAnimated:NO completion:^{
//        }];
//    }];
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:errorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    });
}

-(void)PCPepiCore:(PCPepiCore *)pepiCore progressInfo:(NSString *)info progress:(float)progress
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        self.labelInfo.text = info;
        
        self.progressInfo.progress = progress;
        
//        [self.labelInfo setNeedsDisplay];
//        [self.labelInfo setNeedsLayout];
//        [self.labelInfo layoutIfNeeded];

    });
}


@end
