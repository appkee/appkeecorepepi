//
//  ApplicationController.h
//  pepi
//
//  Created by Radek Zmeskal on 06/12/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PepiCore/PepiCore.h>

@interface ApplicationController : UIViewController<UIAlertViewDelegate, PCPepiCoreDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UIProgressView *progressStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
