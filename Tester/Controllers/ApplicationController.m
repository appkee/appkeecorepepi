//
//  ApplicationController.m
//  pepi
//
//  Created by Radek Zmeskal on 06/12/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "ApplicationController.h"
#import "DataManager.h"
#import "AppDelegate.h"
//#import "NotificationController.h"

@interface ApplicationController ()

@property PCPepiCore *pepiCore;

@end

@implementation ApplicationController

- (void)viewDidLoad {
    [super viewDidLoad];;
    // Do any additional setup after loading the view.
    
#ifdef APPCODE
    if (APPCODE == 101)
    {
        self.progressIndicator.color = [UIColor blackColor];
        self.labelStatus.textColor = [UIColor blackColor];
        self.progressStatus.tintColor = [UIColor blackColor];
        self.progressStatus.trackTintColor = [UIColor grayColor];
    }        
    
    NSNumber *appCode = [NSNumber numberWithInt:APPCODE];
    self.imageView.image = [UIImage imageNamed:@"loader"];

    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"PepiStoryboard" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"infoController"];
    
    self.pepiCore = [[PCPepiCore alloc] initWithAppCode:appCode tester:NO infoController:vc];
    self.pepiCore.delegate = self;
    
#endif
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.pepiCore createController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)PCPepiCore:(PCPepiCore *)pepiCore controllerCreated:(UIViewController *)controller
{
    
    [self presentViewController:controller animated:NO completion:nil];
}

-(void)PCPepiCore:(PCPepiCore *)pepiCore reload:(NSObject *)result
{
    [self.pepiCore createController];
}

-(void)PCPepiCore:(PCPepiCore *)pepiCore close:(NSObject *)result
{
    
}


-(void)PCPepiCore:(PCPepiCore *)pepiCore errorTitle:(NSString *)errorTitle errorMessage:(NSString*)errorMessage retry:(BOOL)retry
{
        dispatch_async(dispatch_get_main_queue(), ^(void){
    
    UIAlertView *alert;
    
    if (retry)
    {
        alert = [[UIAlertView alloc] initWithTitle:errorTitle message:errorMessage delegate:self cancelButtonTitle:@"Zkusit znovu" otherButtonTitles:nil, nil];
    }
    else {
        alert = [[UIAlertView alloc] initWithTitle:errorTitle message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
    
    [alert show];
        });
}

-(void)PCPepiCore:(PCPepiCore *)pepiCore progressInfo:(NSString *)info progress:(float)progress
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        self.labelStatus.text = info;
        
        self.progressStatus.progress = progress;
        
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.pepiCore createController];
}

@end
