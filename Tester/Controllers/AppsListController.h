//
//  AppsListController.h
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuActionController.h"

@interface AppsListController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchResultsUpdating, UIViewControllerPreviewingDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** list of apps */
@property NSArray *appList;

/**
 *  click to reload apps
 *
 *  @param sender button
 */
-(IBAction)clickReload:(id)sender;

/**
 *  click logout apps
 *
 *  @param sender button
 */
-(IBAction)clickLogout:(id)sender;

@end
