//
//  AppsListController.m
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "AppsListController.h"
#import "LoginController.h"
#import "App.h"
#import "AppController.h"
#import "DataManager.h"
#import "Constants.h"

@interface AppsListController ()

@property ( strong, nonatomic) MenuActionController *actionMenu;

@property NSArray *filteredApps;

@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation AppsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init nav bar
    
    self.actionMenu = [[MenuActionController alloc] initWithViewController:self];
    
    // init views
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self performSegueWithIdentifier:@"segueLogin" sender:self];
    
    
    // No search results controller to display the search results in the current view
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    [self.searchController.searchBar sizeToFit];
    
//    self.searchController.searchBar.scopeButtonTitles = @[@"Starts", @"Any"];
    
    self.searchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.definesPresentationContext = YES;
    
    self.tableView.contentOffset = CGPointMake( 0, self.searchController.searchBar.frame.size.height);
    
    return;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchController.active)
    {
        return self.filteredApps.count;
    }
    else {
        return self.appList.count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellApp" forIndexPath:indexPath];
    
    UILabel *labelName = (UILabel*)[cell viewWithTag:-1];
    
    App *app = nil;
    if (self.searchController.active)
    {
        app = self.filteredApps[indexPath.row];
    }
    else {
        app = self.appList[indexPath.row];
    }
    
    labelName.text = app.name;
    
    cell.tag = indexPath.row;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark -
#pragma mark === UISearchBarDelegate ===
#pragma mark -

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

#pragma mark -
#pragma mark === UISearchResultsUpdating ===
#pragma mark -

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    [self searchForText:searchString scope:searchController.searchBar.selectedScopeButtonIndex];
    [self.tableView reloadData];
}

- (void)searchForText:(NSString *)searchText scope:(NSInteger)scopeOption
{
    NSString *predicateStr = nil;
    if (scopeOption == 0)
    {
        predicateStr = [NSString stringWithFormat:@"SELF.name beginswith[c] '%@'", searchText];
    }
    if (scopeOption == 1)
    {
        predicateStr = [NSString stringWithFormat:@"SELF.name CONTAINS[c] '%@a'", searchText];
    }
    
    NSPredicate *bPredicate =[NSPredicate predicateWithFormat:predicateStr];
    self.filteredApps = [self.appList filteredArrayUsingPredicate:bPredicate];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.destinationViewController isKindOfClass:[LoginController class]])
    {
        LoginController *controller = segue.destinationViewController;
        controller.appsListController = self;
        
        return;
    }
    
    if ([segue.destinationViewController isKindOfClass:[AppController class]])
    {
        AppController *controller = segue.destinationViewController;
        NSLog(@"%ld", (long)[sender tag]);
        
        App *app;
        if (self.searchController.active)
        {
            app = self.filteredApps[[sender tag]];        
        }
        else {
            app = self.appList[[sender tag]];
        }
        
        controller.appCode = app.identificator;
    }
}

#pragma mark - IBAction


-(IBAction)clickReload:(id)sender
{
    // reload data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *login = [defaults stringForKey:LOGIN];
    NSString *pass = [defaults stringForKey:PASSWORD];
    
    // send login
    [[DataManager sharedManager] loginWithController:self login:login password:pass success:^(NSArray *apps)
     {
         self.appList = apps;
         
         [self.tableView reloadData];
     } failure:^{
         // show login
         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
         
         [defaults setBool:NO forKey:LOGED];
         [defaults setBool:NO forKey:AUTOLOGIN];
         
         [defaults synchronize];
         
         [self performSegueWithIdentifier:@"segueLogin" sender:self];
     }];
}

-(IBAction)clickLogout:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setBool:NO forKey:LOGED];
    [defaults setBool:NO forKey:AUTOLOGIN];
    [defaults setObject:nil forKey:LOGIN];
    [defaults setObject:nil forKey:PASSWORD];
    
    [defaults synchronize];
    
    self.appList = nil;
    
    [self performSegueWithIdentifier:@"segueLogin" sender:self];
}

@end
