//
//  LoginController.h
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppsListController;

@interface LoginController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textLogin;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UILabel *loginRemember;
@property (weak, nonatomic) IBOutlet UISwitch *switchRemember;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;

/** apps list controlller */
@property AppsListController *appsListController;

@end
