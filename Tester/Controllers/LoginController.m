//
//  LoginController.m
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "LoginController.h"
#import "Constants.h"
#import "DataManager.h"
#import "AppsListController.h"
#import "NSString+MD5.h"

@interface LoginController ()

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textLogin.leftViewMode = UITextFieldViewModeAlways;
    self.textLogin.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pepi_login"]];
    
    self.textPassword.leftViewMode = UITextFieldViewModeAlways;
    self.textPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pepi_password"]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL autologin = [defaults boolForKey:AUTOLOGIN];
    
    // disable login
    [defaults setBool:NO forKey:LOGED];
    
    [defaults synchronize];
    
    // test autologin
    if (autologin)
    {
        // set up login
        NSString *login = [defaults stringForKey:LOGIN];
        NSString *pass = [defaults stringForKey:PASSWORD];
        
        self.textLogin.text = login;
        self.textPassword.text = pass;
        
        // login
        [[DataManager sharedManager] loginWithController:self login:login password:pass success:^(NSArray *apps)
        {
#pragma option DELEGATE
            self.appsListController.appList = apps;
            
            [self dismissViewControllerAnimated:YES completion:nil];
        } failure:^{
            
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBActions

/**
 *  click on return key
 *
 *  @param sender button
 */
-(IBAction)clickReturn:(id)sender
{
    if (self.textLogin == sender)
    {
        [self.textPassword becomeFirstResponder];
    }
    if (self.textPassword == sender)
    {
        [self.textPassword resignFirstResponder];
    }
}

/**
 *  resign keyboard
 *
 *  @param sender button
 */
-(IBAction)resignResponder:(id)sender
{
    [self.textLogin resignFirstResponder];
    [self.textPassword resignFirstResponder];
}


/**
 *  click on login
 *
 *  @param sender button
 */
-(IBAction)clickLogin:(id)sender
{
    // resign keyboard
    [self.textLogin resignFirstResponder];
    [self.textPassword resignFirstResponder];
    
    NSString *login = self.textLogin.text;
    NSString *pass = self.textPassword.text;
    
    // test validation
    if (![login isValidEmail])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Chyba" message:@"Prosím zkontrolujte email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return;
    }
    
    if (pass.length <= 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Chyba" message:@"Prosím zkontrolujte heslo" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return;
    }
    
    // login
    [[DataManager sharedManager] loginWithController:self login:login password:pass success:^(NSArray *apps)
    {
        // save login
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setBool:self.switchRemember.isOn forKey:AUTOLOGIN];
        [defaults setObject:login forKey:LOGIN];
        [defaults setObject:pass forKey:PASSWORD];
        [defaults setBool:YES forKey:LOGED];
        
        [defaults synchronize];
        
#pragma option DELEGATE
        self.appsListController.appList = apps;                
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } failure:^{
        
    }];
}

@end
