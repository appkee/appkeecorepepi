//
//  DataManager.h
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AppDescription;

/**
 * Data manager class
 */
@interface DataManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (id)sharedManager;

/**
 *  login of user
 *
 *  @param viewController actual controller
 *  @param login          login
 *  @param password       password
 *  @param success        success block
 *  @param errorHandler   error handler block
 */
-(void)loginWithController:(UIViewController*) viewController login:(NSString *) login password:(NSString *) password success:(void (^)(NSArray* days)) success failure:(void (^)())errorHandler;



@end
