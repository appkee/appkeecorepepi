//
//  DataManager.m
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "DataManager.h"

#import "MBProgressHUD.h"

#import "NetworkManager.h"
#import "App.h"
#import "FileManager.h"

#define NEW_USER @"new_user"

@interface DataManager ()

//@property ( atomic) BOOL loaded;


@end

@implementation DataManager

+ (id)sharedManager
{
    static DataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)loginWithController:(UIViewController *)viewController login:(NSString *)login password:(NSString *)password success:(void (^)(NSArray *))success failure:(void (^)())errorHandler;
{
    // show progress
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:[viewController view]];
    [[viewController view] addSubview:hud];
    hud.color = [UIColor colorWithRed:0 green:153.0/255.0 blue:204.0/255.0 alpha:1.0];
    
    [hud show:YES];
    
    // request
    [[NetworkManager sharedManager] getApplicationsWithLogin:login password:password success:^(NSDictionary * result)
     {
         NSNumber *status = [result objectForKey:@"login_status"];
         
         if (![status boolValue])
         {
             [hud hide:YES];
             
             // show error message
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Chyba přihlášení" message:@"Zkontrolujte prosím přihlašovací údaje" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [alert show];
             
             errorHandler();
             
             return;
         }
         
         NSMutableArray *apps = [[NSMutableArray alloc] init];
         NSArray *applikace = [result objectForKey:@"aplikace"];
         
         for (NSDictionary *dict in applikace)
         {
             NSError* error = nil;
             App *app = [[App alloc] initWithDictionary:dict error:&error];
             [apps addObject:app];
         }
         
//         [[GraphicManager sharedManager] cleanFoldersArray:apps];
         
         success(apps);
         
         [hud hide:YES];
         
     } failure:^(NSError *error) {
         [hud hide:YES];
         
         // show error message
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedFailureReason] message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         
         [alert show];
         
         errorHandler();
     }];
    
}


@end
