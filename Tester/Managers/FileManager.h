//
//  FileManager.h
//  pepi
//
//  Created by Radek Zmeskal on 12/22/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (FileManager*)sharedManager;


-(void)writeDictionary:(NSDictionary*) dataDictionary;

-(NSDictionary*)loadDictionary;


@end
