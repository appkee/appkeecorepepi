//
//  FileManager.m
//  pepi
//
//  Created by Radek Zmeskal on 12/22/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "FileManager.h"

@interface FileManager ()

@property ( strong, nonatomic) NSString *defaultDirectory;
@property ( strong, nonatomic) NSString *dataFile;

@end

NSString *const file = @"dataFile.txt";

@implementation FileManager

+ (FileManager*)sharedManager
{
    static FileManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        self.defaultDirectory = [paths objectAtIndex:0];
        self.dataFile = [self.defaultDirectory stringByAppendingPathComponent:file];
    }
    return self;
}

-(void)writeDictionary:(NSDictionary *)dataDictionary
{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dataDictionary options:0 error:&err];
//    NSString * dataString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    if (err) {
        return;
    }
    
    [jsonData writeToFile:self.dataFile atomically:YES];
//    [dataString writeToFile:self.dataFile atomically:YES encoding:NSUTF8StringEncoding error:&err];
}

-(NSDictionary*)loadDictionary
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.dataFile])
    {
        return nil;
    }
    
    NSError *error;
    
//    NSString *dataString = [NSString stringWithContentsOfFile:self.dataFile encoding:NSUTF8StringEncoding error:&error];
//    
//    if (error) {
//        return nil;
//    }
    
    NSData *data =[NSData dataWithContentsOfFile:self.dataFile options:NSDataReadingMapped error:&error];
    
    if ((error) || (data == nil))
    {
        return nil;
    }
    
    NSDictionary *dictionary = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error)
    {
        return nil;
    }
    
    return dictionary;
}

@end
