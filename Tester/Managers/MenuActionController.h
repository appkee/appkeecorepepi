//
//  MenuActionController.h
//  pepi
//
//  Created by Radek Zmeskal on 26/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/**
 *  Menu action class
 */
@interface MenuActionController : NSObject <UIActionSheetDelegate>

/**
 *  init menu action on wiew contrller
 *
 *  @param viewController actual view controller
 *  @param type           type of menu
 *
 *  @return instance
 */
-(instancetype)initWithViewController:(UIViewController *)viewController;

@end
