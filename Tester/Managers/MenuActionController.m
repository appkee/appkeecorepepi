//
//  MenuActionController.m
//  pepi
//
//  Created by Radek Zmeskal on 26/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "MenuActionController.h"
#import "AppsListController.h"

@interface MenuActionController ()

@property ( strong, nonatomic) UIViewController *controller;

@property ( strong, nonatomic) UIActionSheet *actionSheet;

@end



@implementation MenuActionController

-(instancetype)initWithViewController:(UIViewController *)viewController
{
    self = [super init];
    if (self)
    {
        self.controller = viewController;
        
        UIBarButtonItem *barButtonMenu = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"pepi_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(clickMenu:)];
//        [barButtonMenu setTintColor:[GraphicManager sharedManager].headerTextColor];
        
        
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                      delegate:self
                                             cancelButtonTitle:@"Zrušit"
                                        destructiveButtonTitle:@"Odhlásit se"
                                             otherButtonTitles:@"Aktualizovat seznam", nil];
            
            [barButtonMenu setTintColor:[UIColor whiteColor]];
        
        self.actionSheet.tintColor = [UIColor whiteColor];
        
        viewController.navigationItem.rightBarButtonItem = barButtonMenu;
                
        self.actionSheet.delegate = self;
    }
    
    return self;
}

#pragma mark - IBActions

-(IBAction)clickMenu:(id)sender
{
    [self.actionSheet showInView:self.controller.view];
}

#pragma mark -action sheets delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (buttonIndex == 0)
        {
            [self.controller performSelector:@selector(clickLogout:) withObject:nil];
        }
        if (buttonIndex == 1)
        {
            [self.controller performSelector:@selector(clickReload:) withObject:nil];
        }
    
}

@end
