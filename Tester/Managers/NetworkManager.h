//
//  NetworkManager.h
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 * Network handler class
 */
@interface NetworkManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (id)sharedManager;

/**
 *  get list of applications
 *
 *  @param login        fogin of user
 *  @param password     password of user
 *  @param success      success block
 *  @param errorHandler error handler block
 */
-(void)getApplicationsWithLogin:(NSString *) login password:(NSString *) password success:(void (^)(NSDictionary *result))success failure:(void (^)(NSError *))errorHandler;



@end
