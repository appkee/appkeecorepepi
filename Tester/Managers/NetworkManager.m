//
//  NetworkManager.m
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import "NetworkManager.h"
#import "NSString+MD5.h"
#import "AFNetworking.h"


#define URL_APPS @"http://pepiapp.cz/app-manager/device-data-loader/get-tester-apps.php"


@implementation NetworkManager

+ (id)sharedManager
{
    static NetworkManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)getApplicationsWithLogin:(NSString *)login password:(NSString *)password success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
//    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:login, @"login", [password MD5String] , @"pass", nil];
    
    [manager POST:URL_APPS parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
         NSData *data = responseObject;
         
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
//         NSLog(@"%@", json);
         
         success(json);
         
     } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         errorHandler(error);
     }];
    
//    NSLog(@"%@", manager);
    
}


@end
