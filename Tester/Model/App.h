//
//  App.h
//  pepi
//
//  Created by Radek Zmeskal on 11/11/15.
//  Copyright © 2015 Tripon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>

@interface App : JSONModel

@property (nonatomic, strong) NSNumber *identificator;
@property (nonatomic, strong) NSString *name;


@end
