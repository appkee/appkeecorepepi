//
//  AppDelegate.h
//  Tester
//
//  Created by Radek Zmeskal on 2/14/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <Google/CloudMessaging.h>
#import <UIKit/UIKit.h>

@interface TesterDelegate : UIResponder <UIApplicationDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate, UIAlertViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, strong) NSString *registrationKey;
@property(nonatomic, strong) NSString *messageKey;
@property(nonatomic, strong) NSString *gcmSenderID;
@property(nonatomic, strong) NSDictionary *registrationOptions;

@property(nonatomic, strong) void (^registrationHandler)
(NSString *registrationToken, NSError *error);
@property(nonatomic, assign) BOOL connectedToGCM;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic;

- (void)subscribeToTopicWithCode:(NSInteger) appCode;

@end

