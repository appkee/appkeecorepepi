//
//  main.m
//  Tester
//
//  Created by Radek Zmeskal on 2/14/16.
//  Copyright © 2016 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TesterDelegate.h"

int main(int argc, char * argv[]) {
    
#ifdef APPCODE
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
#else
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TesterDelegate class]));
    }
#endif
    
}
